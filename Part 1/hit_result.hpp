#ifndef HIT_RESULT_HPP
#define HIT_RESULT_HPP

namespace raytracer
{
	enum class hit_result {
		hit = 1,
		miss = 0,
		in_primitive = -1
	};
}

#endif // HIT_RESULT_HPP
