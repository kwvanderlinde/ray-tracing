#ifndef MATERIAL_HPP
#define MATERIAL_HPP

#include "color.hpp"

namespace raytracer
{
	class material
	{
	private:
		color m_color;
		float m_diff;

	public:
		material() noexcept : m_color{0.2f, 0.2f, 0.2f}, m_diff{0.2f}
		{
		}

		class color& color() noexcept
		{
			return m_color;
		}

		class color const& color() const noexcept
		{
			return m_color;
		}

		void color(class color const & color) noexcept
		{
			m_color = color;
		}

		float diffuse() const noexcept
		{
			return m_diff;
		}

		void diffuse(float diff) noexcept
		{
			m_diff = diff;
		}
	};
}

#endif // MATERIAL_HPP
