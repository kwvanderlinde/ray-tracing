#ifndef PRIMITIVE_HPP
#define PRIMITIVE_HPP

#include "hit_result.hpp"
#include "material.hpp"
#include "params.hpp"
#include "plane.hpp"
#include "ray.hpp"

#include <vlinde/memory/make_unique.hpp>

#include <cmath>
#include <string>
#include <memory>

namespace raytracer
{
	using params::inout;

	class primitive
	{
	public:
		class strategy_t
		{
		public:
      strategy_t() = default;

      strategy_t(strategy_t const &) = default;

			virtual ~strategy_t() = default;

			virtual ::std::unique_ptr<strategy_t> clone() const = 0;

			virtual hit_result intersect(ray const& ray,
			                             inout<float> dist) const
			  noexcept = 0;

			virtual vector3 get_normal_at(vector3 const& position) const
			  noexcept = 0;
		};

	private:
		material m_material;
		::std::string m_name;
		bool m_is_light;
		::std::unique_ptr<strategy_t> m_strategy;

		static ::std::unique_ptr<strategy_t>
		clone(::std::unique_ptr<strategy_t> const& strategy)
		{
			if (strategy == nullptr)
				return nullptr;

			return strategy->clone();
		}

		primitive(::std::unique_ptr<strategy_t> strategy)
		  : m_material{}, m_name{""}, m_is_light{false},
		    m_strategy{ ::std::move(strategy)}
		{
		}

	public:
		primitive() : primitive{nullptr}
		{
		}

		primitive(strategy_t const& strategy) : primitive{strategy.clone()}
		{
		}

		primitive(primitive const& other)
		  : m_material{other.m_material}, m_name{other.m_name},
		    m_is_light{other.m_is_light},
		    m_strategy{clone(other.m_strategy)}
		{
		}

		primitive(primitive&&) = default;

    primitive& operator=(primitive const& other)
    {
      primitive temp{other};
      swap(*this, temp);
      return *this;
    }

		primitive& operator=(primitive&&) = default;

    friend void swap(primitive & first, primitive & second) noexcept
    {
      using std::swap;

      swap(first.m_material, second.m_material);
      swap(first.m_name, second.m_name);
      swap(first.m_is_light, second.m_is_light);
      swap(first.m_strategy, second.m_strategy);
    }

		strategy_t* strategy() noexcept
		{
			return m_strategy.get();
		}

		strategy_t const* strategy() const noexcept
		{
			return m_strategy.get();
		}

		class material& material() noexcept
		{
			return m_material;
		}

		class material const& material() const noexcept
		{
			return m_material;
		}

		void material(class material const& material) noexcept
		{
			m_material = material;
		}

		::std::string const& name() const
		{
			return m_name;
		}

		void name(std::string const& name)
		{
			m_name = name;
		}

		bool is_light() const noexcept
		{
			return m_is_light;
		}

		void is_light(bool is_light) noexcept
		{
			m_is_light = is_light;
		}

		hit_result intersect(ray const& ray, inout<float> dist) const
		  noexcept
		{
			if (m_strategy == nullptr)
				return hit_result::miss;

			return m_strategy->intersect(ray, dist);
		}

		vector3 get_normal_at(vector3 const& position) const noexcept
		{
			if (m_strategy == nullptr)
				return vector3{0, 0, 0};

			return m_strategy->get_normal_at(position);
		}
	};

	class sphere_strategy : public primitive::strategy_t
	{
	private:
		vector3 m_center;
		float m_radius;

		// To avoid recomputations
		float m_square_radius;
		float m_inverse_radius;

	public:
		sphere_strategy(vector3 const& center, float radius)
		  : m_center{center}, m_radius{radius},
		    m_square_radius{radius * radius}, m_inverse_radius{1.f / radius}
		{
		}

		vector3 const& center() const noexcept
		{
			return m_center;
		}

		void center(vector3 const& center) noexcept
		{
			m_center = center;
		}

		float radius() const noexcept
		{
			return m_radius;
		}

		void radius(float radius) noexcept
		{
			m_radius = radius;
			m_square_radius = radius * radius;
			m_inverse_radius = 1.f / radius;
		}

    virtual ::std::unique_ptr<primitive::strategy_t> clone() const
    {
      return ::vlinde::memory::make_unique<sphere_strategy>(*this);
    }

		virtual hit_result intersect(ray const& ray,
		                             inout<float> dist) const
		  noexcept override
    {
      auto const v = ray.origin() - m_center;
      auto const b = -v.dot(ray.direction());
      auto det = (b * b) - v.square_length() + m_square_radius;

      if (det < 0)
        return hit_result::miss;

      det = ::std::sqrt(det);
      auto const i1 = b - det;
      auto const i2 = b + det;

      if (i2 < 0)
        return hit_result::miss;

      if (i1 >= dist)
        return hit_result::miss;

      if (i1 >= 0) {
        dist = i1;
        return hit_result::hit;
      }

      if (i2 < dist) {
        dist = i2;
        return hit_result::in_primitive;
      }

      return hit_result::miss;
    }


		virtual vector3 get_normal_at(vector3 const& position) const
		  noexcept override
    {
      auto result = position - m_center;
      result.normalize();
      return result;
    }
	};

	class plane_strategy : public primitive::strategy_t
	{
	private:
		plane m_plane;

	public:
		plane_strategy(plane const& plane) : m_plane{plane}
		{
		}

		plane const& getPlane() const
		{
			return m_plane;
		}

    virtual ::std::unique_ptr<primitive::strategy_t> clone() const
    {
      return ::vlinde::memory::make_unique<plane_strategy>(*this);
    }

		virtual hit_result intersect(ray const& ray,
		                             inout<float> dist) const
		  noexcept override
    {
      auto const d = m_plane.normal().dot(ray.direction());
      if (d == 0)
        return hit_result::miss;

      auto const normal_projection = m_plane.normal().dot(ray.origin());
      auto const distance = -(normal_projection + m_plane.depth()) / d;

      if (distance < 0 || distance >= dist)
        return hit_result::miss;

      dist = distance;
      return hit_result::hit;
    }

		virtual vector3 get_normal_at(vector3 const&) const
		  noexcept override
    {
      return m_plane.normal();
    }
	};
}

#endif // PRIMITIVE_HPP
