#ifndef COMMON_HPP
#define COMMON_HPP

#include <cstddef>

constexpr float epsilon = 0.0001f;
constexpr ::std::size_t trace_depth = 6;
constexpr float pi = 3.141592653589793238462f;

#endif // COMMON_HPP
