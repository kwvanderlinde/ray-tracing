#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "common.hpp"
#include "params.hpp"
#include "scene.hpp"
#include "surface.hpp"

#include <boost/range/adaptor/filtered.hpp>

#include <cassert>
#include <iostream>
#include <memory>

namespace raytracer
{
	class engine
	{
	private:
		float m_min_x, m_min_y, m_max_x, m_max_y;
		float m_delta_x, m_delta_y, m_current_x, m_current_y;

		surface m_target;
		scene m_scene;

		void init_render()
		{
			m_min_x = -4;
			m_max_x = 4;
			m_min_y = 3;
			m_max_y = -3;
			m_delta_x = (m_max_x - m_min_x) / (m_target.width() - 1);
			m_delta_y = (m_max_y - m_min_y) / (m_target.height() - 1);

			m_current_x = m_min_x;
			m_current_y = m_min_y;
		}

	public:
		surface& render_target() noexcept
		{
			return m_target;
		}

		surface const& render_target() const noexcept
		{
			return m_target;
		}

		void render_target(surface const& target)
		{
			m_target = target;
		}

		void render_target(surface&& target)
		{
			m_target = ::std::move(target);
		}

		class scene& scene() noexcept
		{
			return m_scene;
		}

		class scene const& scene() const noexcept
		{
			return m_scene;
		}

		void scene(class scene const& scene)
		{
			m_scene = scene;
		}

		void scene(class scene&& scene)
		{
			m_scene = ::std::move(scene);
		}

		// Currently non-reentrant!
		void render()
		{
			init_render();

			vector3 const origin{0, 0, -5};

			m_current_y = m_min_y;
			for (surface::size_type y = 0; y < m_target.height(); ++y) {
				m_current_x = m_min_x;

				for (surface::size_type x = 0; x < m_target.width(); ++x) {
					color accumulator{0, 0, 0};

					auto direction =
					  vector3(m_current_x, m_current_y, 0) - origin;
					direction.normalize();

					ray ray{origin, direction};

					// fire the ray.
					raytrace(ray, accumulator, 1);

					// fill the surface buffer.
					m_target(x, y) = accumulator;

					m_current_x += m_delta_x;
				}

				m_current_y += m_delta_y;
			}
		}

		void raytrace(ray const& ray, params::inout<color> accumulator,
		              ::std::size_t depth)
		{
			if (depth > trace_depth)
				return;

			float dist = 1e6;

			primitive const* nearest = nullptr;
			for (auto const& primitive : m_scene) {
				auto result = primitive.intersect(ray, dist);
				if (result != hit_result::miss) {
					nearest = ::std::addressof(primitive);
				}
			}

			if (nearest == nullptr)
				return;

			if (nearest->is_light()) {
				accumulator = color{1, 1, 1};
				return;
			}

			vector3 const intersection_point =
			  ray.origin() + ray.direction() * dist;

			// calculate lighting
			auto const light_filter = ::boost::adaptors::filtered([](
			  primitive const& prim) { return prim.is_light(); });

			for (auto const& light : m_scene | light_filter) {
				auto strategy =
				  static_cast<sphere_strategy const*>(light.strategy());
				assert(strategy != nullptr);

				vector3 L = strategy->center() - intersection_point;
				vector3 N = nearest->get_normal_at(intersection_point);

				// Calculate shadows
				float shade = 1;
				// TODO handle area lights.
				{
					auto tdist = L.length();
					L.normalize();
					class ray r(intersection_point + L * epsilon, L);
					for (auto const& prim : m_scene) {
						if (&prim == &light)
							continue;

						if (prim.intersect(r, tdist) != hit_result::miss) {
							shade = 0;
							break;
						}
					}
				}

				// Calculate diffuse lighting.
				if (nearest->material().diffuse() > 0) {
					L.normalize();
					auto dot = N.dot(L);
					if (dot > 0) {
						auto diff = dot * nearest->material().diffuse() * shade;
						accumulator = accumulator + diff *
						                              nearest->material().color() *
						                              light.material().color();
					}
				}

				// Calculate specular highlight.
				// Reflect the light ray.
				vector3 R = L - 2 * L.dot(N) * N;
				auto dot = ray.direction().dot(R);
				if (dot > 0) {
					auto spec = ::std::pow(dot, 20.f) *
					            nearest->material().specular() * shade;
					accumulator = accumulator + spec * light.material().color();
				}
			}

			// trace reflected ray.
			float refl = nearest->material().reflection();
			if (refl > 0) {
				auto N = nearest->get_normal_at(intersection_point);
				auto R = ray.direction() - 2 * ray.direction().dot(N) * N;

				if (depth < trace_depth) {
					color result{0, 0, 0};
					class ray reflected(intersection_point + epsilon * R, R);
					raytrace(reflected, result, depth + 1);
					accumulator =
					  accumulator + refl * result * nearest->material().color();
				}
			}
		}
	};
}

#endif // ENGINE_HPP
