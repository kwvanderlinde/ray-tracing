#ifndef MATERIAL_HPP
#define MATERIAL_HPP

#include "color.hpp"

namespace raytracer
{
	class material
	{
	private:
		color m_color;
		float m_diff;
		float m_refl;

	public:
		material() noexcept : m_color{0.2f, 0.2f, 0.2f},
		                      m_diff{0.2f},
		                      m_refl{0}
		{
		}

		class color& color() noexcept
		{
			return m_color;
		}

		class color const& color() const noexcept
		{
			return m_color;
		}

		void color(class color const& color) noexcept
		{
			m_color = color;
		}

		float diffuse() const noexcept
		{
			return m_diff;
		}

		void diffuse(float diff) noexcept
		{
			m_diff = diff;
		}

		float reflection() const noexcept
		{
			return m_refl;
		}

		void reflection(float refl) noexcept
		{
			m_refl = refl;
		}

		float specular() const noexcept
		{
			return 1 - m_diff;
		}
	};
}

#endif // MATERIAL_HPP
