#ifndef PARAMS_HPP
#define PARAMS_HPP

#include <memory>

namespace params
{
	template <typename T>
	class out_base
	{
	protected:
		T* m_object;

	public:
    out_base(T& object)
    : m_object{::std::addressof(object)}
    {
    }
	};

	template <typename T>
	class out : private out_base<T>
	{
    using base = out_base<T>;

	public:
		out(T& object)
    : base{object}
		{
		}

		void operator=(T const& value)
		{
			*(this->m_object) = value;
		}
	};

  template<typename T>
  class inout : private out_base<T>
  {
    using base = out_base<T>;

  public:
    inout(T& object)
    : base{object}
    {
    }

		void operator=(T const& value)
		{
			*(this->m_object) = value;
		}

    T const & value() const
    {
      return *(this->m_object);
    }

    operator T const &() const
    {
      return this->value();
    }
  };

  template<typename T>
  class in
  {
    T const & m_value;

  public:
    in(T const& value)
    : m_value{value}
    {
    }

    T const & value() const
    {
      return m_value;
    }

    operator T const &() const
    {
      return this->value();
    }
  };

}

#endif // PARAMS_HPP
