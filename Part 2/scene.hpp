#ifndef SCENE_HPP
#define SCENE_HPP

#include "primitive.hpp"

#include <vlinde/memory/make_unique.hpp>

#include <cassert>
#include <initializer_list>
#include <utility>
#include <vector>

namespace raytracer
{
	class scene
	{
	private:
		::std::vector<primitive> m_primitives;

	public:
		using value_type = primitive;
		using reference = value_type&;
		using const_reference = value_type const&;

		using iterator = decltype(m_primitives)::iterator;
		using const_iterator = decltype(m_primitives)::const_iterator;

		using difference_type =
		  ::std::iterator_traits<iterator>::difference_type;
		using size_type = ::std::make_unsigned<difference_type>::type;

		scene() = default;

		scene(scene const&) = default;

		scene(scene&&) = default;

		scene& operator=(scene const&) = default;

		scene& operator=(scene&&) = default;

		void add(primitive const& primitive)
		{
			m_primitives.push_back(primitive);
		}

		void add(primitive&& primitive)
		{
			m_primitives.push_back(::std::move(primitive));
		}

		size_type size() const noexcept
		{
			return m_primitives.size();
		}

		size_type max_size() const noexcept
		{
			return m_primitives.max_size();
		}

		bool empty() const noexcept
		{
			return m_primitives.empty();
		}

		void swap(scene& other)
		{
			using std::swap;
			swap(m_primitives, other.m_primitives);
		}

		friend void swap(scene& first, scene& second)
		{
      first.swap(second);
		}

		primitive& operator[](size_type index) noexcept
		{
			assert(index < m_primitives.size());

			return m_primitives[index];
		}

		primitive const& operator[](size_type index) const noexcept
		{
			assert(index < m_primitives.size());

			return m_primitives[index];
		}

		iterator begin()
		{
			return m_primitives.begin();
		}

		const_iterator begin() const
		{
			return m_primitives.begin();
		}

		const_iterator cbegin() const
		{
			return m_primitives.cbegin();
		}

		iterator end()
		{
			return m_primitives.end();
		}

		const_iterator end() const
		{
			return m_primitives.end();
		}

		const_iterator cend() const
		{
			return m_primitives.cend();
		}
	};
}

#endif // SCENE_HPP
