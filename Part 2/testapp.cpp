#include "camera.hpp"
#include "color.hpp"
#include "common.hpp"
#include "engine.hpp"
#include "hit_result.hpp"
#include "material.hpp"
#include "params.hpp"
#include "plane.hpp"
#include "primitive.hpp"
#include "ray.hpp"
#include "scene.hpp"
#include "surface.hpp"
#include "vector3.hpp"

#include <fstream>
#include <type_traits>

raytracer::scene makeScene()
{
	using namespace raytracer;

	scene scene;

	{
		primitive back_plane{plane_strategy{{vector3{0, 1, 0}, 4.4f}}};
		back_plane.name("plane");
    back_plane.material().reflection(0);
		back_plane.material().diffuse(1);
		back_plane.material().color({0.4f, 0.3f, 0.3f});
		scene.add(::std::move(back_plane));
	}

	{
		primitive big_sphere{sphere_strategy{vector3{1, -0.8f, 3}, 2.5f}};
		big_sphere.name("big sphere");
    big_sphere.material().reflection(0.6f);
		big_sphere.material().color({0.7f, 0.7f, 0.7f});
		scene.add(::std::move(big_sphere));
	}

	{
		primitive small_sphere{sphere_strategy{vector3{-5.5, -0.5f, 7}, 2}};
		small_sphere.name("small sphere");
		small_sphere.material().reflection(1.f);
		small_sphere.material().diffuse(0.1f);
		small_sphere.material().color({0.7f, 0.7f, 1.0f});
		scene.add(::std::move(small_sphere));
	}

	{
		primitive light1{sphere_strategy{vector3{0, 5, 5}, 0.1f}};
		light1.is_light(true);
		light1.material().color({0.6f, 0.6f, 0.6f});
		scene.add(::std::move(light1));
	}

	{
		primitive light2{sphere_strategy{vector3{2, 5, 1}, 0.1f}};
		light2.is_light(true);
		light2.material().color({0.7f, 0.7f, 0.9f});
		scene.add(::std::move(light2));
	}

	return scene;
}

int main()
{
	using namespace raytracer;

	auto const width = 800;
	auto const height = 600;

	// Setup the rendering engine.
	engine engine;
	engine.render_target(surface{width, height});
	engine.scene(makeScene());

	// render the scene.
	engine.render();

	// write the image to a pbm file.
	::std::ofstream out{"test.ppm"};
	out << "P3" << ::std::endl << engine.render_target().width() << " "
	    << engine.render_target().height() << ::std::endl << "255"
	    << ::std::endl;

	for (auto const& color : engine.render_target()) {
		auto red = static_cast<int>(255.f * color.red());
		auto green = static_cast<int>(255.f * color.green());
		auto blue = static_cast<int>(255.f * color.blue());

		red = ::std::max(0, ::std::min(red, 255));
		green = ::std::max(0, ::std::min(green, 255));
		blue = ::std::max(0, ::std::min(blue, 255));

		out << red << " " << green << " " << blue << " ";
	}

	return 0;
}
