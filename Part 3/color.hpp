#ifndef COLOR_HPP
#define COLOR_HPP

#include "common.hpp"
#include "vector3.hpp"

#include <boost/strong_typedef.hpp>

namespace raytracer
{
	class color
	{
	private:
		std::array<float, 3> m_impl;

	public:
		color() noexcept : m_impl{}
		{
		}

		color(float x, float y, float z) noexcept : m_impl{{x, y, z}}
		{
		}

		float red() const noexcept
		{
			return ::std::get<0>(m_impl);
		}

		void red(float red) noexcept
		{
			::std::get<0>(m_impl) = red;
		}

		float green() const noexcept
		{
			return ::std::get<1>(m_impl);
		}

		void green(float green) noexcept
		{
			::std::get<1>(m_impl) = green;
		}

		float blue() const noexcept
		{
			return ::std::get<2>(m_impl);
		}

		void blue(float blue) noexcept
		{
			::std::get<2>(m_impl) = blue;
		}

		color& operator+=(color const& other) noexcept
		{
			::boost::transform(m_impl, other.m_impl, m_impl.begin(),
			                   ::std::plus<float>{});
			return *this;
		}

		color& operator-=(color const& other) noexcept
		{
			::boost::transform(m_impl, other.m_impl, m_impl.begin(),
			                   ::std::minus<float>{});
			return *this;
		}

		color& operator*=(float f) noexcept
		{
			for (auto& x : m_impl)
				x *= f;
			return *this;
		}

		color& operator*=(color const& other) noexcept
		{
			::boost::transform(m_impl, other.m_impl, m_impl.begin(),
			                   ::std::multiplies<float>{});
			return *this;
		}

    color& operator/=(float f) noexcept
    {
      for (auto& x : m_impl)
        x /= f;
      return *this;
    }

		color& operator/=(color const& other) noexcept
		{
			::boost::transform(m_impl, other.m_impl, m_impl.begin(),
			                   ::std::divides<float>{});
			return *this;
		}

		color operator-() const noexcept
		{
			color result;
			::boost::transform(m_impl, result.m_impl.begin(),
			                   ::std::negate<float>{});
			return result;
		}

		friend color operator+(color const& a, color const& b) noexcept
		{
			color result{a};
			result += b;
			return result;
		}

		friend color operator-(color const& a, color const& b) noexcept
		{
			color result{a};
			result -= b;
			return result;
		}

		friend color operator*(color const& a, float f) noexcept
		{
			color result{a};
			result *= f;
			return result;
		}

		friend color operator*(color const& a, color const& b) noexcept
		{
			color result{a};
			result *= b;
			return result;
		}

		friend color operator*(float f, color const& a) noexcept
		{
			color result{a};
			result *= f;
			return result;
		}

		friend color operator/(color const& a, float f) noexcept
		{
			color result{a};
			result /= f;
			return result;
		}

		friend color operator/(color const& a, color const& b) noexcept
		{
			color result{a};
			result /= b;
			return result;
		}

		friend color operator/(float f, color const& a) noexcept
		{
			color result{a};
			result /= f;
			return result;
		}
	};
}

#endif // COLOR_HPP
