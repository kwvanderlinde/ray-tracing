#ifndef COMMON_HPP
#define COMMON_HPP

#include <cstddef>
#include <tuple>

constexpr float epsilon = 0.001f;
constexpr ::std::size_t trace_depth = 6;
constexpr float pi = 3.141592653589793238462f;

static auto& _ = ::std::ignore;

#endif // COMMON_HPP
