#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "common.hpp"
#include "params.hpp"
#include "scene.hpp"
#include "surface.hpp"

#include <boost/range/adaptor/filtered.hpp>
#include <boost/range/irange.hpp>
#include <vlinde/memory/make_unique.hpp>

#include <algorithm>
#include <cassert>
#include <iostream>
#include <memory>
#include <tuple>

namespace raytracer
{
	class engine
	{
	private:
		float m_min_x, m_min_y, m_max_x, m_max_y;
		float m_delta_x, m_delta_y, m_current_x, m_current_y;

		surface m_target;
		scene m_scene;
		::std::unique_ptr<primitive const * []> m_last_prim_line;
		primitive const* m_last_prim;

		void init_render()
		{
			m_min_x = -4;
			m_max_x = 4;
			m_min_y = 3;
			m_max_y = -3;
			m_delta_x = (m_max_x - m_min_x) / (m_target.width() - 1);
			m_delta_y = (m_max_y - m_min_y) / (m_target.height() - 1);

			m_current_x = m_min_x;
			m_current_y = m_min_y;

			m_last_prim_line =
			  ::vlinde::memory::make_unique<primitive const * []>(
			    m_target.width());

			// This should happen by default, but I'm not convinced right now.
			::std::fill_n(m_last_prim_line.get(), m_target.width(), nullptr);
			m_last_prim = nullptr;
		}

	public:
		surface& render_target() noexcept
		{
			return m_target;
		}

		surface const& render_target() const noexcept
		{
			return m_target;
		}

		void render_target(surface const& target)
		{
			m_target = target;
		}

		void render_target(surface&& target)
		{
			m_target = ::std::move(target);
		}

		class scene& scene() noexcept
		{
			return m_scene;
		}

		class scene const& scene() const noexcept
		{
			return m_scene;
		}

		void scene(class scene const& scene)
		{
			m_scene = scene;
		}

		void scene(class scene&& scene)
		{
			m_scene = ::std::move(scene);
		}

		// Currently non-reentrant!
		void render()
		{
			init_render();

			vector3 const origin{0, 0, -5};

			m_current_y = m_min_y;
			for (surface::size_type y = 0; y < m_target.height(); ++y) {
				m_current_x = m_min_x;

				for (surface::size_type x = 0; x < m_target.width(); ++x) {

					auto direction =
					  vector3(m_current_x, m_current_y, 0) - origin;
					direction.normalize();

					ray const ray{origin, direction};

					// fire the ray.
					color resultColor;
					primitive const* prim;
					::std::tie(resultColor, _, prim) = raytrace(ray, 1, 1.f);

					// Supersample at primitive boundaries.
					if (prim != m_last_prim || prim != m_last_prim_line[x]) {
						color color;
						// 3x3 supersampling.
						for (auto tx : ::boost::irange(-1, 2)) {
							for (auto ty : ::boost::irange(-1, 2)) {
								if (tx == 0 && ty == 0)
									continue;

								auto dir =
								  vector3(m_current_x + m_delta_x * tx / 2.f,
								          m_current_y + m_delta_y * ty / 2.f, 0) -
								  origin;
								dir.normalize();
								class ray r(origin, dir);
								::std::tie(color, _, _) = raytrace(ray, 1, 1.f);
								resultColor += color;
							}
						}
						resultColor /= 9.f;
					}

					// cache the current primitive.
					m_last_prim_line[x] = prim;
					m_last_prim = prim;

					// fill the surface buffer.
					m_target(x, y) = resultColor;

					m_current_x += m_delta_x;
				}

				m_current_y += m_delta_y;
			}
		}

		/*
		 * returns: rendered color, distance travelled, and hit primitie.
		 */
		::std::tuple<color, float, primitive const*>
		raytrace(ray const& ray, ::std::size_t const depth,
		         float const index)
		{
			if (depth > trace_depth)
				return ::std::make_tuple(color{0, 0, 0}, 0, nullptr);

			float distance = 1e6;

			primitive const* nearest = nullptr;
			hit_result hit{hit_result::miss};
			for (auto const& primitive : m_scene) {
				auto result = primitive.intersect(ray, distance);
				if (result != hit_result::miss) {
					nearest = ::std::addressof(primitive);
					hit = result;
				}
			}

			if (nearest == nullptr)
				return ::std::make_tuple(color{0, 0, 0}, distance, nullptr);

			if (nearest->is_light())
				return ::std::make_tuple(color{1, 1, 1}, distance, nearest);

			vector3 const intersection_point =
			  ray.origin() + ray.direction() * distance;
			vector3 const N = nearest->get_normal_at(intersection_point);

			color resultColor{0, 0, 0};

			// calculate lighting
			auto const light_filter = ::boost::adaptors::filtered([](
			  primitive const& prim) { return prim.is_light(); });

			for (auto const& light : m_scene | light_filter) {
				auto strategy =
				  static_cast<sphere_strategy const*>(light.strategy());
				assert(strategy != nullptr);

				vector3 const fullL = strategy->center() - intersection_point;
				vector3 const L = fullL * (1 / fullL.length());

				// Calculate shadows
				float shade = 1;
				// TODO handle area lights.
				{
					auto tdist = fullL.length();
					class ray r(intersection_point + L * epsilon, L);
					for (auto const& prim : m_scene) {
						if (&prim == &light)
							continue;

						if (prim.intersect(r, tdist) != hit_result::miss) {
							shade = 0;
							break;
						}
					}
				}

				if (shade <= 0)
					continue;

				// Calculate diffuse lighting.
				if (nearest->material().diffuse() > 0) {
					auto dot = N.dot(L);
					if (dot > 0) {
						auto diff = dot * nearest->material().diffuse() * shade;
						resultColor += diff * nearest->material().color() *
						               light.material().color();
					}
				}

				// Calculate specular highlight.
				// Reflect the light ray.
				vector3 R = L - 2 * L.dot(N) * N;
				auto dot = ray.direction().dot(R);
				if (dot > 0) {
					auto spec = ::std::pow(dot, 20.f) *
					            nearest->material().specular() * shade;
					resultColor += spec * light.material().color();
				}
			}

			// trace reflected ray.
			float refl = nearest->material().reflection();
			if (refl > 0 && depth < trace_depth) {
				auto R = ray.direction() - 2 * ray.direction().dot(N) * N;

				class ray reflected(intersection_point + epsilon * R, R);

				color result;
				::std::tie(result, _, _) =
				  raytrace(reflected, depth + 1, index);
				resultColor += refl * result * nearest->material().color();
			}

			// trace refracted ray.
			float refr = nearest->material().refraction();
			if (refr > 0 && depth < trace_depth) {
				float rindex = nearest->material().refraction_index();
				auto n = index / rindex;

				auto flipped = hit == hit_result::in_primitive ? -N : N;

				auto cosI = -flipped.dot(ray.direction());
				auto cosT2 = 1 - n * n * (1 - cosI * cosI);
				if (cosT2 > 0) {
					auto T = n * ray.direction() +
					         (n * cosI - ::std::sqrt(cosT2)) * flipped;
					color result;
					float dist;
					class ray refracted(intersection_point + T * epsilon, T);
					::std::tie(result, dist, _) =
					  raytrace(refracted, depth + 1, rindex);

					// Apply Beer's law.
					auto absorbance = -nearest->material().color() * 0.15f * dist;
					color transparency{ ::std::exp(absorbance.red()),
					                    ::std::exp(absorbance.green()),
					                    ::std::exp(absorbance.blue())};
					resultColor += result * transparency;
				}
			}

			return ::std::make_tuple(resultColor, distance, nearest);
		}
	};
}

#endif // ENGINE_HPP
