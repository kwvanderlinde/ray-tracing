#ifndef MATERIAL_HPP
#define MATERIAL_HPP

#include "color.hpp"

namespace raytracer
{
	class material
	{
	private:
		color m_color;
		float m_diff;
		float m_spec;
		float m_refl;
		float m_refr;
		float m_rindex;

	public:
		material() noexcept : m_color{0.2f, 0.2f, 0.2f},
		                      m_diff{0.2f},
		                      m_spec{0.8f},
		                      m_refl{0},
		                      m_refr{0},
		                      m_rindex{1.5f}
		{
		}

		class color& color() noexcept
		{
			return m_color;
		}

		class color const& color() const noexcept
		{
			return m_color;
		}

		void color(class color const& color) noexcept
		{
			m_color = color;
		}

		float diffuse() const noexcept
		{
			return m_diff;
		}

		void diffuse(float diff) noexcept
		{
			m_diff = diff;
		}

		float reflection() const noexcept
		{
			return m_refl;
		}

		void reflection(float refl) noexcept
		{
			m_refl = refl;
		}

		float refraction() const noexcept
		{
			return m_refr;
		}

		void refraction(float refr) noexcept
		{
			m_refr = refr;
		}

		float refraction_index() const noexcept
		{
			return m_rindex;
		}

		void refraction_index(float index) noexcept
		{
			m_rindex = index;
		}

		float specular() const noexcept
		{
			return m_spec;
		}

		void specular(float spec) noexcept
		{
			m_spec = spec;
		}
	};
}

#endif // MATERIAL_HPP
