#ifndef PLANE_HPP
#define PLANE_HPP

#include "vector3.hpp"

namespace raytracer
{
	class plane
	{
	private:
		vector3 m_normal;
		float m_depth;

	public:
		plane() noexcept : m_normal{0, 0, 0}, m_depth{0}
		{
		}

		plane(vector3 normal, float depth) noexcept : m_normal{normal},
		                                              m_depth{depth}
		{
		}

		vector3& normal() noexcept
		{
			return m_normal;
		}

		vector3 const& normal() const noexcept
		{
			return m_normal;
		}

		void normal(vector3 const& normal) noexcept
		{
			m_normal = normal;
		}

		float depth() const noexcept
		{
			return m_depth;
		}

		void depth(float depth) noexcept
		{
			m_depth = depth;
		}
	};
}

#endif // PLANE_HPP
