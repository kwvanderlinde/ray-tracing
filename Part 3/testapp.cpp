#include "camera.hpp"
#include "color.hpp"
#include "common.hpp"
#include "engine.hpp"
#include "hit_result.hpp"
#include "material.hpp"
#include "params.hpp"
#include "plane.hpp"
#include "primitive.hpp"
#include "ray.hpp"
#include "scene.hpp"
#include "surface.hpp"
#include "vector3.hpp"

#include <boost/range/irange.hpp>

#include <fstream>
#include <type_traits>

raytracer::scene makeScene()
{
	using namespace raytracer;

	scene scene;

	{
		primitive floor_plane{plane_strategy{{vector3{0, 1, 0}, 4.4f}}};
		floor_plane.name("floor plane");
		floor_plane.material().reflection(0);
		floor_plane.material().refraction(0);
		floor_plane.material().diffuse(1);
		floor_plane.material().color({0.4f, 0.3f, 0.3f});
		scene.add(::std::move(floor_plane));
	}

	{
		primitive back_plane{plane_strategy{{vector3{0.4f, 0, -1}, 12}}};
		back_plane.name("back plane");
		back_plane.material().reflection(0);
		back_plane.material().refraction(0);
		back_plane.material().specular(0);
		back_plane.material().diffuse(0.6f);
		back_plane.material().color({0.5f, 0.3f, 0.5f});
		scene.add(::std::move(back_plane));
	}

	{
		primitive ceiling_plane{plane_strategy{{vector3{0, -1, 0}, 7.4f}}};
		ceiling_plane.name("ceiling plane");
		ceiling_plane.material().reflection(0);
		ceiling_plane.material().refraction(0);
		ceiling_plane.material().specular(0);
		ceiling_plane.material().diffuse(0.5f);
		ceiling_plane.material().color({0.4f, 0.7f, 0.7f});
		scene.add(::std::move(ceiling_plane));
	}

	{
		primitive big_sphere{sphere_strategy{vector3{2, 0.8f, 3}, 2.5f}};
		big_sphere.name("big sphere");
		big_sphere.material().reflection(0.2f);
		big_sphere.material().refraction(0.8f);
		big_sphere.material().refraction_index(1.3f);
		big_sphere.material().color({0.7f, 0.7f, 1.0f});
		scene.add(::std::move(big_sphere));
	}

	{
		primitive small_sphere{sphere_strategy{vector3{-5.5, -0.5f, 7}, 2}};
		small_sphere.name("small sphere");
		small_sphere.material().reflection(0.5f);
		small_sphere.material().refraction(0);
		small_sphere.material().refraction_index(1.3f);
		small_sphere.material().diffuse(0.1f);
		small_sphere.material().color({0.7f, 0.7f, 1.0f});
		scene.add(::std::move(small_sphere));
	}

	{
		primitive extra_sphere{
		  sphere_strategy{vector3{-1.5, -3.8f, 1}, 1.5f}};
		extra_sphere.name("extra sphere");
		extra_sphere.material().reflection(0);
		extra_sphere.material().refraction(0.8f);
		extra_sphere.material().color({1, 0.4f, 0.4f});
		scene.add(::std::move(extra_sphere));
	}

	{
		primitive light1{sphere_strategy{vector3{0, 5, 5}, 0.1f}};
		light1.is_light(true);
		light1.material().color({0.4f, 0.4f, 0.4f});
		scene.add(::std::move(light1));
	}

	{
		primitive light2{sphere_strategy{vector3{-3, 5, 1}, 0.1f}};
		light2.is_light(true);
		light2.material().color({0.6f, 0.6f, 0.8f});
		scene.add(::std::move(light2));
	}

	// Make a grid of tiny spheres.
	for (auto x : ::boost::irange(0, 8)) {
		for (auto y : ::boost::irange(0, 7)) {
			primitive sphere{sphere_strategy{
			  vector3{-4.5f + x * 1.5f, -4.3f + y * 1.5f, 10}, 0.3f}};
      sphere.name("grid sphere");
      sphere.material().reflection(0);
      sphere.material().refraction(0);
      sphere.material().specular(0.6f);
      sphere.material().diffuse(0.6f);
      sphere.material().color({0.3f, 1.f, 0.4f});
      scene.add(sphere);
		}
	}

	return scene;
}

int main()
{
	using namespace raytracer;

	auto const width = 800;
	auto const height = 600;

	// Setup the rendering engine.
	engine engine;
	engine.render_target(surface{width, height});
	engine.scene(makeScene());

	// render the scene.
	engine.render();

	// write the image to a pbm file.
	::std::ofstream out{"test.ppm"};
	out << "P3" << ::std::endl << engine.render_target().width() << " "
	    << engine.render_target().height() << ::std::endl << "255"
	    << ::std::endl;

	for (auto const& color : engine.render_target()) {
		auto red = static_cast<int>(255.f * color.red());
		auto green = static_cast<int>(255.f * color.green());
		auto blue = static_cast<int>(255.f * color.blue());

		red = ::std::max(0, ::std::min(red, 255));
		green = ::std::max(0, ::std::min(green, 255));
		blue = ::std::max(0, ::std::min(blue, 255));

		out << red << " " << green << " " << blue << " ";
	}

	return 0;
}
