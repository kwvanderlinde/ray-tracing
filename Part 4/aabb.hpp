#ifndef AABB_HPP
#define AABB_HPP

#include "common.hpp"
#include "ray.hpp"
#include "vector3.hpp"

#include <boost/range/irange.hpp>

namespace raytracer
{
	class aabb
	{
	private:
		vector3 m_position;
		vector3 m_size;

	public:
		aabb() = default;

		aabb(vector3 const& position, vector3 const& size)
		  : m_position{position}, m_size{size}
		{
		}

		vector3 const& position() const noexcept
		{
			return m_position;
		}

		vector3 const& size() const noexcept
		{
			return m_size;
		}

		bool intersect(aabb const& b2) const noexcept
		{
			auto const& v1 = b2.position();
			auto v2 = v1 + b2.size();
			auto const& v3 = this->position();
			auto v4 = v3 + this->size();

			return ((v4.x() > v1.x()) && (v3.x() < v2.x()) &&
			        (v4.y() > v1.y()) && (v3.y() < v2.y()) &&
			        (v4.z() > v1.z()) && (v3.z() < v2.z()));
		}

		bool contains(vector3 const& v) const noexcept
		{
			auto const& v1 = this->position();
			auto v2 = v1 + this->size();

			return (v.x() > v1.x() - epsilon) && (v.x() < v2.x() + epsilon) &&
			       (v.y() > v1.y() - epsilon) && (v.y() < v2.y() + epsilon) &&
			       (v.z() > v1.z() - epsilon) && (v.z() < v2.z() + epsilon);
		}

		void advanceRay(ray& ray) const noexcept
		{
			float min_distance = 1e6;
			float distances[6]{-1, -1, -1, -1, -1, -1};
			vector3 intersections[6];

			auto const& direction = ray.direction();
			auto const& origin = ray.origin();

			auto const& v1 = this->position();
			auto const& v2 = this->position() + this->size();

			if (direction.x() != 0) {
				distances[0] = (v1.x() - origin.x()) / direction.x();
				distances[3] = (v2.x() - origin.x()) / direction.x();
			}
			if (direction.y() != 0) {
				distances[0] = (v1.y() - origin.y()) / direction.y();
				distances[3] = (v2.y() - origin.y()) / direction.y();
			}
			if (direction.z() != 0) {
				distances[0] = (v1.z() - origin.z()) / direction.z();
				distances[3] = (v2.z() - origin.z()) / direction.z();
			}

			for (auto i : ::boost::irange(0, 6)) {
				auto& v = intersections[i] = origin + distances[i] * direction;

				if ((v.x() > v1.x() - epsilon) && (v.x() < v2.x() + epsilon) &&
				    (v.y() > v1.y() - epsilon) && (v.y() < v2.y() + epsilon) &&
				    (v.z() > v1.z() - epsilon) && (v.z() < v2.z() + epsilon)) {
					if (distances[i] < min_distance) {
						min_distance = distances[i];
					}
				}
			}

			ray.origin(ray.origin() +
			           (min_distance + epsilon) * ray.direction());
		}
	};
}

#endif // AABB_HPP
