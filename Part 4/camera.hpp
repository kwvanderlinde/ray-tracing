#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "common.hpp"
#include "vector3.hpp"

#include <cmath>

namespace raytracer
{
	class camera
	{
	private:
		vector3 m_position;
		vector3 m_lookat;
		vector3 m_up;
		vector3 m_forward;
		vector3 m_left;

		float m_fov_y;
		float m_aspect;
		float m_distance;

		// The world size of half the viewing area.
		float m_delta_y;
		float m_delta_x;

	public:
		camera(vector3 const& position, vector3 const& lookat,
		       vector3 const& up, float fov_y, float aspect)
		  : m_position{position}, m_lookat{lookat}, m_up{vector3{up}},
		    m_forward{lookat - position}, m_left{up.cross(m_forward)},
		    m_fov_y{fov_y}, m_aspect{aspect},
		    m_distance{m_forward.length()},
		    m_delta_y{m_distance * ::std::tan(m_fov_y / 2)},
		    m_delta_x{m_delta_y * m_aspect}
		{
			// normalize all our directional vectors.
			m_up.normalize();
			m_forward.normalize();
			m_left.normalize();
		}

		vector3 getUpperLeft() const noexcept
		{
			return m_lookat + m_left * m_delta_x + m_up * m_delta_y;
		}
	};
}

#endif // CAMERA_HPP
