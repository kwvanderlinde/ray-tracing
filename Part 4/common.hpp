#ifndef COMMON_HPP
#define COMMON_HPP

#include <cstddef>
#include <tuple>

constexpr ::std::size_t grid_size = 8;
constexpr float epsilon = 0.001f;
constexpr float pi = 3.141592653589793238462f;

static auto& _ = ::std::ignore;

template <typename T>
constexpr T const& clamp(T const& a, T const& min, T const& max)
{
	return (a < min) ? min : ((a > max) ? max : a);
}

#endif // COMMON_HPP
