#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "common.hpp"
#include "params.hpp"
#include "scene.hpp"
#include "surface.hpp"

#include <boost/optional.hpp>
#include <boost/range/adaptor/filtered.hpp>
#include <boost/range/irange.hpp>
#include <vlinde/memory/make_unique.hpp>

#include <algorithm>
#include <cassert>
#include <iostream>
#include <memory>

namespace raytracer
{
	class engine
	{
	private:
		float m_min_x, m_min_y, m_max_x, m_max_y;
		float m_delta_x, m_delta_y, m_current_x, m_current_y;
		::std::unique_ptr<primitive const * []> m_last_prim_line;
		primitive const* m_last_prim;

		surface m_target;
		scene m_scene;
		::std::size_t m_trace_depth;

		void init_render()
		{
			m_min_x = -4;
			m_max_x = 4;
			m_min_y = 3;
			m_max_y = -3;
			m_delta_x = (m_max_x - m_min_x) / (m_target.width() - 1);
			m_delta_y = (m_max_y - m_min_y) / (m_target.height() - 1);

			m_current_x = m_min_x;
			m_current_y = m_min_y;

			m_last_prim_line =
			  ::vlinde::memory::make_unique<primitive const * []>(
			    m_target.width());

			// This should happen by default, but I'm not convinced right now.
			::std::fill_n(m_last_prim_line.get(), m_target.width(), nullptr);
			m_last_prim = nullptr;
		}

	public:
		engine() : m_target{}, m_scene{}, m_trace_depth{1}
		{
		}

		surface& render_target() noexcept
		{
			return m_target;
		}

		surface const& render_target() const noexcept
		{
			return m_target;
		}

		void render_target(surface const& target)
		{
			m_target = target;
		}

		void render_target(surface&& target)
		{
			m_target = ::std::move(target);
		}

		class scene& scene() noexcept
		{
			return m_scene;
		}

		class scene const& scene() const noexcept
		{
			return m_scene;
		}

		void scene(class scene&& scene)
		{
			m_scene = ::std::move(scene);
		}

		::std::size_t trace_depth() const noexcept
		{
			return m_trace_depth;
		}

		void trace_depth(::std::size_t trace_depth) noexcept
		{
			m_trace_depth = trace_depth;
		}

		struct raytrace_result
		{
			color color;
			hit hit;
		};

		// Currently non-reentrant!
		void render()
		{
			init_render();

			m_current_y = m_min_y;
			for (surface::size_type y = 0; y < m_target.height(); ++y) {
				m_current_x = m_min_x;

				for (surface::size_type x = 0; x < m_target.width(); ++x) {
					// fire the ray.
					auto result =
					  render_ray(vector3{m_current_x, m_current_y, 0});

					// Supersample at primitive boundaries.
					if (result.hit.primitive != m_last_prim ||
					    result.hit.primitive != m_last_prim_line[x]) {
						// 3x3 supersampling.
						for (auto tx : ::boost::irange(-1, 2)) {
							for (auto ty : ::boost::irange(-1, 2)) {
								if (tx == 0 && ty == 0)
									continue;

								auto const sx = m_current_x + m_delta_x * tx / 2.f;
								auto const sy = m_current_y + m_delta_y * ty / 2.f;
								auto const vec = vector3{sx, sy, 0};
								result.color += render_ray(vec).color;
							}
						}
						result.color /= 9.f;
					}

					// cache the current primitive.
					m_last_prim_line[x] = result.hit.primitive;
					m_last_prim = result.hit.primitive;

					// fill the surface buffer.
					m_target(x, y) = result.color;

					m_current_x += m_delta_x;
				}

				m_current_y += m_delta_y;
			}
		}

	private:
		hit find_nearest_in(ray const& ray, float const distance,
		                    cell const& cell) const noexcept
		{
			hit result{nullptr, distance, inside::no};

			for (auto const& primitive : cell.primitives) {
				if (auto hit_result =
				      primitive->intersect(ray, result.distance)) {
					result = *hit_result;
				}
			}

			return result;
		}

		hit find_nearest(ray const& ray, float const max_distance) const
		  noexcept
		{
			hit result{nullptr, max_distance, inside::no};

			// Setup our control variables.

			auto const& grid = m_scene.grid();
			auto const& cell_size = grid.cell_size();
			::std::size_t step_x, step_y, step_z;
			::std::size_t x, y, z;

			{
				auto cell =
				  (ray.origin() - grid.extents().position()) / cell_size;

				if ((cell.x() < 0 || cell.x() >= grid.size_x()) ||
				    (cell.y() < 0 || cell.y() >= grid.size_y()) ||
				    (cell.z() < 0 || cell.z() >= grid.size_z()))
					return result;

				x = static_cast<decltype(x)>(cell.x());
				y = static_cast<decltype(y)>(cell.y());
				z = static_cast<decltype(z)>(cell.z());
			}

			vector3 cell_bound;
			if (ray.direction().x() > 0) {
				step_x = 1;
				cell_bound.x(1);
			} else {
				step_x = ::std::numeric_limits<::std::size_t>::max();
				cell_bound.x(0);
			}

			if (ray.direction().y() > 0) {
				step_y = 1;
				cell_bound.y(1);
			} else {
				step_y = ::std::numeric_limits<::std::size_t>::max();
				cell_bound.y(0);
			}

			if (ray.direction().z() > 0) {
				step_z = 1;
				cell_bound.z(1);
			} else {
				step_z = ::std::numeric_limits<::std::size_t>::max();
				cell_bound.z(0);
			}

			{
				auto const& extents = grid(x, y, z).extents;
				cell_bound = extents.position() + cell_bound * extents.size();
			}

			auto tmax = cell_bound - ray.origin();
			auto tdelta = cell_size / ray.direction();
			if (ray.direction().x() != 0) {
				tmax.x(tmax.x() / ray.direction().x());
				if (step_x != 1)
					// We're going towards negative x.
					tdelta.x(-tdelta.x());
			} else {
				tmax.x(1e6);
			}

			if (ray.direction().y() != 0) {
				tmax.y(tmax.y() / ray.direction().y());
				if (step_y != 1)
					// We're going towards negative y.
					tdelta.y(-tdelta.y());
			} else {
				tmax.y(1e6);
			}

			if (ray.direction().z() != 0) {
				tmax.z(tmax.z() / ray.direction().z());
				if (step_z != 1)
					// We're going towards negative z.
					tdelta.z(-tdelta.z());
			} else {
				tmax.z(1e6);
			}

			// Step through the grid.
			while (true) {
				auto const& cell = grid(x, y, z);
				auto temp = result;
				auto nearest = find_nearest_in(ray, temp.distance, cell);
				if (nearest.primitive != nullptr)
					temp = nearest;

				// Step to next cell.
				if (tmax.x() <= tmax.y() && tmax.x() <= tmax.z()) {
					if (tmax.x() > temp.distance) {
						result = temp;
						break;
					}

					x += step_x;
					tmax.x(tmax.x() + tdelta.x());

					if (x >= grid.size_x())
						break;
				} else if (tmax.y() <= tmax.x() && tmax.y() <= tmax.z()) {
					if (tmax.y() > temp.distance) {
						result = temp;
						break;
					}

					y += step_y;
					tmax.y(tmax.y() + tdelta.y());

					if (y >= grid.size_y())
						break;
				} else {
					if (tmax.z() > temp.distance) {
						result = temp;
						break;
					}

					z += step_z;
					tmax.z(tmax.z() + tdelta.z());

					if (z >= grid.size_z())
						break;
				}
			}

			return result;
		}

		color colorize(ray const& ray, ::std::size_t depth,
		               float const index, hit const& hit) const noexcept
		{
			color result{0, 0, 0};

			if (hit.primitive == nullptr)
				return result;

			if (hit.primitive->is_light()) {
				return result = {1, 1, 1};
			}

			auto const& material = hit.primitive->material();
			auto const intersection_point =
			  ray.origin() + ray.direction() * hit.distance;
			auto const N = hit.primitive->get_normal_at(intersection_point);

			// calculate lighting
			auto const light_filter = ::boost::adaptors::filtered([](
			  primitive const* prim) { return prim->is_light(); });

			for (auto const& light : m_scene | light_filter) {
				auto lighting_points = light->get_lighting_points();

				if (lighting_points.empty())
					continue;

				// Calculate shadows
				::std::size_t shade_count = lighting_points.size();
				for (auto const& point : lighting_points) {
					auto const light_displacement = point - intersection_point;
					auto const light_distance = light_displacement.length();
					auto const light_direction =
					  light_displacement / light_distance;

					class ray r(intersection_point + light_direction * epsilon,
					            light_direction);

					auto nearest = find_nearest(r, light_distance);
					if (nearest.primitive != nullptr &&
					    nearest.primitive != light) {
						--shade_count;
					}
				}
				auto shade =
				  static_cast<float>(shade_count) / lighting_points.size();
				shade = clamp(shade, 0.f, 1.f);

				if (shade <= 0)
					continue;

				// We'll need these later for the Phong model.
				auto const& mainPoint = lighting_points.front();
				auto const L = (mainPoint - intersection_point).normalize();
				auto const NdotL = N.dot(L);

				// Calculate diffuse lighting.
				if (material.diffuse() > 0 && NdotL > 0) {
					auto diff = NdotL * material.diffuse() * shade;
					result += diff * material.color() * light->material().color();
				}

				// Calculate specular highlight.
				// Reflect the light ray.
				{
					vector3 R = L - 2 * N.dot(L) * N;
					auto dot = ray.direction().dot(R);
					if (dot > 0) {
						auto spec =
						  ::std::pow(dot, 20.f) * material.specular() * shade;
						result += spec * light->material().color();
					}
				}
			}

			// trace reflected ray.
			float refl = material.reflection();
			if (refl > 0 && depth < m_trace_depth) {
				auto R = ray.direction() - 2 * ray.direction().dot(N) * N;

				class ray reflected(intersection_point + epsilon * R, R);

				auto refl_result = raytrace(reflected, depth + 1, index);
				result += refl * refl_result.color * material.color();
			}

			// trace refracted ray.
			float refr = material.refraction();
			if (refr > 0 && depth < m_trace_depth) {
				float rindex = material.refraction_index();
				auto n = index / rindex;

				auto flipped = hit.inside == inside::yes ? -N : N;

				auto cosI = -flipped.dot(ray.direction());
				auto cosT2 = 1 - n * n * (1 - cosI * cosI);
				if (cosT2 > 0) {
					auto T = n * ray.direction() +
					         (n * cosI - ::std::sqrt(cosT2)) * flipped;
					class ray refracted(intersection_point + T * epsilon, T);
					auto refr_result = raytrace(refracted, depth + 1, rindex);

					// Apply Beer's law.
					auto absorbance =
					  -material.color() * 0.15f * refr_result.hit.distance;
					color transparency{ ::std::exp(absorbance.red()),
					                    ::std::exp(absorbance.green()),
					                    ::std::exp(absorbance.blue())};
					result += refr_result.color * transparency;
				}
			}

			return result;
		}

		raytrace_result raytrace(ray const& ray, ::std::size_t const depth,
		                         float const index) const
		{
			raytrace_result result{color{0, 0, 0},
			                       hit{nullptr, 1e6, inside::no}};

			if (depth > m_trace_depth)
				return result;

			result.hit = find_nearest(ray, result.hit.distance);

			result.color = colorize(ray, depth, index, result.hit);

			return result;
		}

	public:
		raytrace_result render_ray(vector3 screen_position) const noexcept
		{
			auto const& extents = m_scene.grid().extents();

			vector3 const origin{0, 0, -5};
			vector3 const direction = (screen_position - origin).normalize();
			ray ray{origin, direction};

			// Advance ray to box boundary.
			if (!extents.contains(origin)) {
				extents.advanceRay(ray);
			}

			return raytrace(ray, 1, 1.f);
		}
	};
}

#endif // ENGINE_HPP
