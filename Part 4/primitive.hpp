#ifndef PRIMITIVE_HPP
#define PRIMITIVE_HPP

#include "aabb.hpp"
#include "hit.hpp"
#include "material.hpp"
#include "plane.hpp"
#include "ray.hpp"

#include <boost/optional.hpp>
#include <boost/range/irange.hpp>
#include <vlinde/memory/make_unique.hpp>

#include <cstddef>
#include <cmath>
#include <string>
#include <memory>
#include <vector>

namespace raytracer
{
	struct intersect_result
	{
		float distance;
		inside inside;
	};

	class primitive
	{
	private:
		material m_material;
		::std::string m_name;
		bool m_is_light;

	public:
		primitive() : m_material{}, m_name{""}, m_is_light{false}
		{
		}

		virtual ~primitive() = default;

		class material& material() noexcept
		{
			return m_material;
		}

		class material const& material() const noexcept
		{
			return m_material;
		}

		void material(class material const& material) noexcept
		{
			m_material = material;
		}

		::std::string const& name() const
		{
			return m_name;
		}

		void name(std::string const& name)
		{
			m_name = name;
		}

		bool is_light() const noexcept
		{
			return m_is_light;
		}

		void is_light(bool is_light) noexcept
		{
			m_is_light = is_light;
		}

	private:
		/**
		 * Obtains a number of points in the primitive which will act as
		 * light sources.
		 *
		 * Subclasses should override this to provide lighting behaviour.
		 * The return value should be some appropriate sampling of points
		 * from the primitive to act as light sources. By default, the empty
		 * vector is returned.
		 *
		 * If the returned range is not empty, then the first point is
		 * considered the "main" or "centroid" point. It will be used in
		 * models which don't use sampling, e.g., the Phong model.
		 */
		// TODO: Somehow make this return a generator rather than a concrete
		// vector, in case many sample points are returned.
		virtual ::std::vector<vector3> get_lighting_points_impl() const
		{
			return {};
		}

	public:
		/**
		 * Obtains a number of points in the primitive which will act as
		 * light sources.
		 *
		 * If `!this->isLight()`, then nothing is returns. Else, the points
		 * returned by `getLightingPointsImpl()` is returned.
		 */
		::std::vector<vector3> get_lighting_points() const
		{
			if (!this->is_light())
				return {};

			return this->get_lighting_points_impl();
		}

		::boost::optional<hit> intersect(ray const& ray, float dist) const
		  noexcept
		{
			auto result = intersect_impl(ray, dist);
			if (!result)
				return {};

			return hit{this, result->distance, result->inside};
		}

	private:
		virtual ::boost::optional<intersect_result>
		intersect_impl(ray const& ray, float dist) const noexcept = 0;

	public:
		virtual vector3 get_normal_at(vector3 const& position) const
		  noexcept = 0;

		virtual aabb get_aabb() const noexcept = 0;

		virtual bool intersects(aabb const& box) const noexcept = 0;
	};

	class sphere_primitive : public primitive
	{
	private:
		vector3 m_center;
		float m_radius;

		// To avoid recomputations
		float m_square_radius;
		float m_inverse_radius;

	public:
		sphere_primitive(vector3 const& center, float radius)
		  : m_center{center}, m_radius{radius},
		    m_square_radius{radius * radius}, m_inverse_radius{1.f / radius}
		{
		}

		vector3 const& center() const noexcept
		{
			return m_center;
		}

		void center(vector3 const& center) noexcept
		{
			m_center = center;
		}

		float radius() const noexcept
		{
			return m_radius;
		}

		void radius(float radius) noexcept
		{
			m_radius = radius;
			m_square_radius = radius * radius;
			m_inverse_radius = 1.f / radius;
		}

		virtual ::std::vector<vector3> get_lighting_points_impl() const
		  override
		{
			return {m_center};
		}

		virtual ::boost::optional<intersect_result>
		intersect_impl(ray const& ray, float const dist) const
		  noexcept override
		{
			auto const v = ray.origin() - m_center;
			auto const b = -v.dot(ray.direction());
			auto det = (b * b) - v.square_length() + m_square_radius;

			if (det < 0)
				return {};

			det = ::std::sqrt(det);
			auto const i1 = b - det;
			auto const i2 = b + det;

			if (i2 < 0)
				return {};

			if (i1 >= dist)
				return {};

			if (i1 >= 0) {
				return intersect_result{i1, inside::no};
			}

			if (i2 < dist) {
				return intersect_result{i2, inside::yes};
			}

			return {};
		}

		virtual vector3 get_normal_at(vector3 const& position) const
		  noexcept override
		{
			return (position - m_center).normalize();
		}

		virtual bool intersects(aabb const& box) const noexcept override
		{
			float dmin = 0;
			auto const& v1 = box.position();
			auto v2 = box.position() + box.size();

			{
				float diff = 0;
				if (m_center.x() < v1.x())
					diff = m_center.x() - v1.x();
				else if (m_center.x() > v2.x())
					diff = m_center.x() - v2.x();
				dmin += diff * diff;
			}

			{
				float diff = 0;
				if (m_center.y() < v1.y())
					diff = m_center.y() - v1.y();
				else if (m_center.y() > v2.y())
					diff = m_center.y() - v2.y();
				dmin += diff * diff;
			}

			{
				float diff = 0;
				if (m_center.z() < v1.z())
					diff = m_center.z() - v1.z();
				else if (m_center.z() > v2.z())
					diff = m_center.z() - v2.z();
				dmin += diff * diff;
			}

			return dmin <= m_square_radius;
		}

		virtual aabb get_aabb() const noexcept override
		{
			vector3 size{m_radius, m_radius, m_radius};
			return {m_center - size, size * 2};
		}
	};

	class plane_primitive : public primitive
	{
	private:
		plane m_plane;

	public:
		plane_primitive(plane const& plane) : m_plane{plane}
		{
		}

		plane const& getPlane() const
		{
			return m_plane;
		}

		virtual ::boost::optional<intersect_result>
		intersect_impl(ray const& ray, float const dist) const
		  noexcept override
		{
			auto const d = m_plane.normal().dot(ray.direction());
			if (d == 0)
				return {};

			auto const normal_projection = m_plane.normal().dot(ray.origin());
			auto const distance = -(normal_projection + m_plane.depth()) / d;

			if (distance < 0 || distance >= dist)
				return {};

			return intersect_result{distance, inside::no};
		}

		virtual vector3 get_normal_at(vector3 const&) const
		  noexcept override
		{
			return m_plane.normal();
		}

		virtual bool intersects(aabb const& box) const noexcept override
		{
			vector3 vs[2]{box.position(), box.position() + box.size()};

			// Some fancy algorithm! I'll just trust it does what it does.
			::std::size_t side1 = 0;
			::std::size_t side2 = 0;
			for (auto k : ::boost::irange(0, 2)) {
				for (auto j : ::boost::irange(0, 2)) {
					for (auto i : ::boost::irange(0, 2)) {
						vector3 point{vs[i].x(), vs[j].y(), vs[k].z()};

						if (point.dot(m_plane.normal()) + m_plane.depth() < 0)
							++side1;
						else
							++side2;
					}
				}
			}

			if (side1 == 0 || side2 == 0)
				return false;

			return true;
		}

		virtual aabb get_aabb() const noexcept override
		{
			return {vector3{-10000, -10000, -10000},
			        vector3{20000, 20000, 20000}};
		}
	};
}

#endif // PRIMITIVE_HPP
