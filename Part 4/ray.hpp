#ifndef RAY_HPP
#define RAY_HPP

#include "vector3.hpp"

namespace raytracer
{
	class ray
	{
	private:
		vector3 m_origin;
		vector3 m_direction;

	public:
		ray() : m_origin{0, 0, 0}, m_direction{0, 0, 0}
		{
		}

		ray(vector3 const& origin, vector3 const& direction)
		  : m_origin{origin}, m_direction{direction}
		{
		}

		vector3& origin() noexcept
		{
			return m_origin;
		}

		vector3 const& origin() const noexcept
		{
			return m_origin;
		}

		void origin(vector3 const& origin) noexcept
		{
			m_origin = origin;
		}

		vector3& direction() noexcept
		{
			return m_direction;
		}

		vector3 const& direction() const noexcept
		{
			return m_direction;
		}

		void direction(vector3 const& direction) noexcept
		{
			m_direction = direction;
		}
	};
}

#endif // RAY_HPP
