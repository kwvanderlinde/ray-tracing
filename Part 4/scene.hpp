#ifndef SCENE_HPP
#define SCENE_HPP

#include "common.hpp"
#include "grid.hpp"
#include "primitive.hpp"

#include <boost/iterator/transform_iterator.hpp>
#include <vlinde/memory/make_unique.hpp>

#include <cassert>
#include <initializer_list>
#include <memory>
#include <utility>
#include <vector>

namespace raytracer
{
	struct get_ptr
	{
		template <typename T>
		T* operator()(::std::unique_ptr<T>& ptr) const
		{
			return ptr.get();
		}

		template <typename T>
		T const* operator()(::std::unique_ptr<T> const& ptr) const
		{
			return ptr.get();
		}
	};

	// TODO Remove modifying methods and non-const primitive access.
	// TODO Add a separate list of lights.
	class scene
	{
	private:
		::std::vector<::std::unique_ptr<primitive>> m_primitives;
		grid m_grid;

	public:
		// Use const transitively.
		using value_type = primitive*;
		using reference = primitive*&;
		using const_reference = primitive const* const&;

		using iterator = ::boost::transform_iterator<
		  get_ptr, decltype(m_primitives)::iterator>;
		using const_iterator = ::boost::transform_iterator<
		  get_ptr, decltype(m_primitives)::const_iterator>;

		using difference_type =
		  ::std::iterator_traits<iterator>::difference_type;
		using size_type = ::std::make_unsigned<difference_type>::type;

		scene()
		  : m_primitives{},
		    m_grid{aabb{{-14, -5, -6}, {28, 13, 36}}, grid_size,
		           grid_size,                         grid_size}
		{
		}

		scene(scene const&) = delete;

		scene(scene&&) = default;

		scene& operator=(scene const&) = delete;

		scene& operator=(scene&&) = default;

		void build_grid()
		{
			auto const& cell_size = m_grid.cell_size();
			auto const& extents = m_grid.extents();
			auto const& p1 = extents.position();

			for (auto primitive : *this) {
				auto bounds = primitive->get_aabb();
				auto const& v1 = bounds.position();
				auto const& v2 = bounds.position() + bounds.size();

				// Find out which cells could contain the primitive.
				auto diff1 = (v1 - p1) / cell_size;
				auto diff2 = (v2 - p1) / cell_size;

				diff1.x(clamp<float>(diff1.x(), 0, m_grid.size_x() - 1));
				diff1.y(clamp<float>(diff1.y(), 0, m_grid.size_y() - 1));
				diff1.z(clamp<float>(diff1.z(), 0, m_grid.size_z() - 1));

				diff2.x(clamp<float>(diff2.x(), 0, m_grid.size_x() - 1));
				diff2.y(clamp<float>(diff2.y(), 0, m_grid.size_y() - 1));
				diff2.z(clamp<float>(diff2.z(), 0, m_grid.size_z() - 1));

				// Get indices from the diffs.
				auto x1 = static_cast<::std::size_t>(diff1.x());
				auto y1 = static_cast<::std::size_t>(diff1.y());
				auto z1 = static_cast<::std::size_t>(diff1.z());

				auto x2 = static_cast<::std::size_t>(diff2.x()) + 1;
				auto y2 = static_cast<::std::size_t>(diff2.y()) + 1;
				auto z2 = static_cast<::std::size_t>(diff2.z()) + 1;

				for (auto z : ::boost::irange(z1, z2)) {
					for (auto y : ::boost::irange(y1, y2)) {
						for (auto x : ::boost::irange(x1, x2)) {
							auto& cell = m_grid(x, y, z);

							// Do an accurate cell/primitive intersection.
							if (primitive->intersects(cell.extents)) {
								// Add the primitive to the cell.
								cell.primitives.push_back(primitive);
							}
						}
					}
				}
			}
		}

		class grid& grid() noexcept
		{
			return m_grid;
		}

		class grid const& grid() const noexcept
		{
			return m_grid;
		}

		void add(::std::unique_ptr<primitive>&& primitive)
		{
			m_primitives.push_back(::std::move(primitive));
		}

		size_type size() const noexcept
		{
			return m_primitives.size();
		}

		size_type max_size() const noexcept
		{
			return m_primitives.max_size();
		}

		bool empty() const noexcept
		{
			return m_primitives.empty();
		}

		void swap(scene& other)
		{
			using std::swap;
			swap(m_primitives, other.m_primitives);
		}

		friend void swap(scene& first, scene& second)
		{
			first.swap(second);
		}

		primitive& operator[](size_type index) noexcept
		{
			assert(index < m_primitives.size());

			return *m_primitives[index];
		}

		primitive const& operator[](size_type index) const noexcept
		{
			assert(index < m_primitives.size());

			return *m_primitives[index];
		}

		iterator begin()
		{
			return {m_primitives.begin(), get_ptr{}};
		}

		const_iterator begin() const
		{
			return {m_primitives.begin(), get_ptr{}};
		}

		const_iterator cbegin() const
		{
			return {m_primitives.cbegin(), get_ptr{}};
		}

		iterator end()
		{
			return {m_primitives.end(), get_ptr{}};
		}

		const_iterator end() const
		{
			return {m_primitives.end(), get_ptr{}};
		}

		const_iterator cend() const
		{
			return {m_primitives.cend(), get_ptr{}};
		}
	};
}

#endif // SCENE_HPP
