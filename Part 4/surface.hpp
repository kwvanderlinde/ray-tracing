#ifndef SURFACE_HPP
#define SURFACE_HPP

#include "color.hpp"

#include <boost/range/algorithm/fill.hpp>
#include <vlinde/memory/make_unique.hpp>

#include <cstddef>
#include <cstring>
#include <iterator>
#include <memory>
#include <utility>

namespace raytracer
{
	// TODO Add text rendering.
	class surface
	{
	private:
    // TODO Handle `m_buffer == nullptr` as a valid state in methods.

		::std::size_t m_width;
		::std::size_t m_height;
		::std::size_t m_size;
		::std::unique_ptr<color[]> m_buffer;

	public:
		using value_type = color;
		using reference = value_type&;
		using const_reference = value_type const&;

		using iterator = color*;
		using const_iterator = color const*;

		using difference_type =
		  ::std::iterator_traits<iterator>::difference_type;
		using size_type = ::std::make_unsigned<difference_type>::type;

    surface()
    : surface{0, 0}
    {
    }

		surface(size_type width, size_type height)
		  : m_width{width}, m_height{height}, m_size{width * height},
		    m_buffer{ ::vlinde::memory::make_unique<color[]>(m_size)}
		{
      this->clear(color{0, 0, 0});
		}

		surface(surface const& other)
		  : surface{other.m_width, other.m_height}
		{
			// no more exceptions may be thrown (not that it matters!)
			// color is trivially copyable
			auto const byte_count = m_size * sizeof(color);
			::std::memcpy(m_buffer.get(), other.m_buffer.get(), byte_count);
		}

		surface(surface&&) noexcept = default;

    surface& operator=(surface const & other)
    {
      // copy and swap.
      surface temp{other};
      this->swap(temp);
      return *this;
    }

		surface& operator=(surface&&) noexcept = default;

		size_type width() const noexcept
		{
			return m_width;
		}

		size_type height() const noexcept
		{
			return m_height;
		}

		size_type size() const noexcept
		{
			return m_size;
		}

		size_type max_size() const noexcept
		{
			return static_cast<size_type>(
			  ::std::numeric_limits<difference_type>::max());
		}

		bool empty() const noexcept
		{
			return m_size == 0;
		}

    color & operator()(size_type x, size_type y) noexcept
    {
      return m_buffer[y * m_width + x];
    }

    color const & operator()(size_type x, size_type y) const noexcept
    {
      return m_buffer[y * m_width + x];
    }

    void clear(color const & color)
    {
      ::boost::fill(*this, color);
    }

    color & operator[](size_type index) noexcept
    {
      return m_buffer[index];
    }

    color const & operator[](size_type index) const noexcept
    {
      return m_buffer[index];
    }

		void swap(surface& other) noexcept
		{
			using std::swap;
			swap(m_buffer, other.m_buffer);
		}

		friend void swap(surface& first, surface& second) noexcept
		{
			first.swap(second);
		}

		iterator begin()
		{
			return m_buffer.get();
		}

		const_iterator begin() const
		{
			return this->cend();
		}

		const_iterator cbegin() const
		{
			return m_buffer.get();
		}

		iterator end()
		{
			return m_buffer.get() + m_size;
		}

		const_iterator end() const
		{
			return this->cend();
		}

		const_iterator cend() const
		{
			return m_buffer.get() + m_size;
		}
	};
}

#endif // SURFACE_HPP
