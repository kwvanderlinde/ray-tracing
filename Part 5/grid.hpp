#ifndef GRID_HPP
#define GRID_HPP

#include "aabb.hpp"
#include "common.hpp"
#include "primitive.hpp"

#include <cassert>
#include <cstddef>
#include <memory>
#include <vector>

namespace raytracer
{
	struct cell
	{
		aabb extents;
		::std::vector<primitive const*> primitives;
	};

	class grid
	{
	private:
		aabb m_extents;
		vector3 m_cell_size;
		::std::size_t m_size_x; // x-direction
		::std::size_t m_size_y; // y-direction
		::std::size_t m_size_z; // z-direction
		::std::size_t m_size;
		::std::unique_ptr<cell[]> m_impl;

	public:
		using size_type = ::std::size_t;

		grid(aabb extents, ::std::size_t size_x, ::std::size_t size_y,
		     ::std::size_t size_z)
		  : m_extents{extents}, m_size_x{size_x}, m_size_y{size_y},
		    m_size_z{size_z}, m_size{m_size_x * m_size_y * m_size_z},
		    m_impl{ ::vlinde::memory::make_unique<cell[]>(m_size)}
		{
			vector3 const voxel_counts(m_size_x, m_size_y, m_size_z);
			m_cell_size = m_extents.size() / voxel_counts;

			for (auto z : ::boost::irange<::std::size_t>(0, m_size_z)) {
				for (auto y : ::boost::irange<::std::size_t>(0, m_size_y)) {
					for (auto x : ::boost::irange<::std::size_t>(0, m_size_x)) {
						vector3 const voxel(x, y, z);
						auto const pos = m_extents.position() + m_cell_size * voxel;
						(*this)(x, y, z).extents = {pos, m_cell_size};
					}
				}
			}
		}

		aabb const& extents() const noexcept
		{
			return m_extents;
		}

		::std::size_t size_x() const noexcept
		{
			return m_size_x;
		}

		::std::size_t size_y() const noexcept
		{
			return m_size_y;
		}

		::std::size_t size_z() const noexcept
		{
			return m_size_z;
		}

		vector3 const& cell_size() const noexcept
		{
			return m_cell_size;
		}

		cell& operator()(size_type x, size_type y, size_type z) noexcept
		{
			assert(x < m_size_x);
			assert(y < m_size_y);
			assert(z < m_size_z);

			return m_impl[x + y * m_size_x + z * m_size_x * m_size_y];
		}

		cell const& operator()(size_type x, size_type y, size_type z) const
		  noexcept
		{
			assert(x < m_size_x);
			assert(y < m_size_y);
			assert(z < m_size_z);

			return m_impl[x + y * m_size_x + z * m_size_x * m_size_y];
		}
	};
}

#endif // GRID_HPP
