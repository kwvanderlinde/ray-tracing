#ifndef HIT_RESULT_HPP
#define HIT_RESULT_HPP

namespace raytracer
{
  class primitive;

	enum class inside : bool {
		no = false,
		yes = true
	};

	struct hit
	{
    primitive const* primitive;
		float distance;
		inside inside;
	};
}

#endif // HIT_RESULT_HPP
