#ifndef PRIMITIVE_HPP
#define PRIMITIVE_HPP

#include "aabb.hpp"
#include "hit.hpp"
#include "material.hpp"
#include "plane.hpp"
#include "ray.hpp"

#include <boost/optional.hpp>
#include <boost/range/irange.hpp>
#include <vlinde/memory/make_unique.hpp>

#include <cstddef>
#include <cmath>
#include <memory>
#include <random>
#include <string>
#include <vector>

namespace raytracer
{
	struct intersect_result
	{
		float distance;
		inside inside;
	};

	/**
	 * Represents lighting information for use in the ray tracing engine.
	 */
	struct light_bundle
	{
		/**
		 * A central representative of the light source.
		 *
		 * This is used to implement non-sampling lighiting models such as
		 * the Phong model.
		 */
		vector3 center;

		/**
		 * A sampling of points of the light source.
		 *
		 * This is used to calculate soft shadows. If empty, hard shadows
		 * are calculated using `center` as the sole sample. Otherwise, soft
		 * shadows are calculated via the proportion of occluded samples.
		 */
		::std::vector<vector3> samples;
	};

	class primitive
	{
	private:
		material m_material;
		::std::string m_name;
		bool m_is_light;

	public:
		primitive() : m_material{}, m_name{""}, m_is_light{false}
		{
		}

		virtual ~primitive() = default;

		class material& material() noexcept
		{
			return m_material;
		}

		class material const& material() const noexcept
		{
			return m_material;
		}

		void material(class material const& material) noexcept
		{
			m_material = material;
		}

		::std::string const& name() const
		{
			return m_name;
		}

		void name(std::string const& name)
		{
			m_name = name;
		}

		bool is_light() const noexcept
		{
			return this->get_is_light();
		}

		void is_light(bool is_light) noexcept
		{
			this->set_is_light(is_light);
		}

	private:
		/**
		 * Overridable implementation of the `is_light` getter.
		 */
		virtual bool get_is_light() const noexcept
		{
			return m_is_light;
		}

		/**
		 * Overridable implementation of the `is_light` setter.
		 */
		virtual void set_is_light(bool is_light) noexcept
		{
			m_is_light = is_light;
		}

		/**
		 * Obtains a number of points in the primitive which will act as
		 * light sources.
		 *
		 * Subclasses should override this to provide lighting behaviour.
		 * The return value should be some appropriate sampling of points
		 * from the primitive to act as light sources. By default, the empty
		 * vector is returned.
		 *
		 * The return value must be meaningful if `this->is_light()`. Thus,
		 * if a subclass is not intended to be used as a light,
		 * `set_is_light` should be overriden to enforce this.
		 */
		// TODO: Somehow make this return a generator rather than a concrete
		// vector, in case many sample points are returned. This could also
		// enable the caller to control the number of samples without
		// explicit dependancy on a configuration parameter.
		virtual light_bundle get_lighting_points_impl() const
		{
			return {vector3{}, {}};
		}

	public:
		/**
		 * Obtains a number of points in the primitive which will act as
		 * light sources.
		 *
		 * If `!this->isLight()`, then nothing is returns. Else, the points
		 * returned by `getLightingPointsImpl()` is returned.
		 */
		light_bundle get_lighting_points() const
		{
			if (!this->is_light())
				return {vector3{0, 0, 0}, {}};

			return this->get_lighting_points_impl();
		}

		::boost::optional<hit> intersect(ray const& ray, float dist) const
		  noexcept
		{
			auto result = intersect_impl(ray, dist);
			if (!result)
				return {};

			return hit{this, result->distance, result->inside};
		}

	private:
		virtual ::boost::optional<intersect_result>
		intersect_impl(ray const& ray, float dist) const noexcept = 0;

	public:
		virtual vector3 get_normal_at(vector3 const& position) const
		  noexcept = 0;

		virtual aabb get_aabb() const noexcept = 0;

		virtual bool intersects(aabb const& box) const noexcept = 0;
	};

	class sphere_primitive : public primitive
	{
	private:
		vector3 m_center;
		float m_radius;

		// To avoid recomputations
		float m_square_radius;
		float m_inverse_radius;

	public:
		sphere_primitive(vector3 const& center, float radius)
		  : m_center{center}, m_radius{radius},
		    m_square_radius{radius * radius}, m_inverse_radius{1.f / radius}
		{
		}

		vector3 const& center() const noexcept
		{
			return m_center;
		}

		void center(vector3 const& center) noexcept
		{
			m_center = center;
		}

		float radius() const noexcept
		{
			return m_radius;
		}

		void radius(float radius) noexcept
		{
			m_radius = radius;
			m_square_radius = radius * radius;
			m_inverse_radius = 1.f / radius;
		}

	private:
		virtual light_bundle get_lighting_points_impl() const override
		{
			return {m_center, {}};
		}

	public:
		virtual ::boost::optional<intersect_result>
		intersect_impl(ray const& ray, float const dist) const
		  noexcept override
		{
			auto const v = ray.origin() - m_center;
			auto const b = -v.dot(ray.direction());
			auto det = (b * b) - v.square_length() + m_square_radius;

			if (det < 0)
				return {};

			det = ::std::sqrt(det);
			auto const i1 = b - det;
			auto const i2 = b + det;

			if (i2 < 0)
				return {};

			if (i1 >= dist)
				return {};

			if (i1 >= 0) {
				return intersect_result{i1, inside::no};
			}

			if (i2 < dist) {
				return intersect_result{i2, inside::yes};
			}

			return {};
		}

		virtual vector3 get_normal_at(vector3 const& position) const
		  noexcept override
		{
			return (position - m_center).normalize();
		}

		virtual bool intersects(aabb const& box) const noexcept override
		{
			float dmin = 0;
			auto const& v1 = box.position();
			auto v2 = box.position() + box.size();

			{
				float diff = 0;
				if (m_center.x() < v1.x())
					diff = m_center.x() - v1.x();
				else if (m_center.x() > v2.x())
					diff = m_center.x() - v2.x();
				dmin += diff * diff;
			}

			{
				float diff = 0;
				if (m_center.y() < v1.y())
					diff = m_center.y() - v1.y();
				else if (m_center.y() > v2.y())
					diff = m_center.y() - v2.y();
				dmin += diff * diff;
			}

			{
				float diff = 0;
				if (m_center.z() < v1.z())
					diff = m_center.z() - v1.z();
				else if (m_center.z() > v2.z())
					diff = m_center.z() - v2.z();
				dmin += diff * diff;
			}

			return dmin <= m_square_radius;
		}

		virtual aabb get_aabb() const noexcept override
		{
			vector3 size{m_radius, m_radius, m_radius};
			return {m_center - size, size * 2};
		}
	};

	class plane_primitive : public primitive
	{
	private:
		plane m_plane;

	public:
		plane_primitive(plane const& plane) : m_plane{plane}
		{
		}

		plane const& getPlane() const
		{
			return m_plane;
		}

	private:
		virtual bool get_is_light() const noexcept override
		{
			return false;
		}

		virtual ::boost::optional<intersect_result>
		intersect_impl(ray const& ray, float const dist) const
		  noexcept override
		{
			auto const d = m_plane.normal().dot(ray.direction());
			if (d == 0)
				return {};

			auto const normal_projection = m_plane.normal().dot(ray.origin());
			auto const distance = -(normal_projection + m_plane.depth()) / d;

			if (distance < 0 || distance >= dist)
				return {};

			return intersect_result{distance, inside::no};
		}

	public:
		virtual vector3 get_normal_at(vector3 const&) const
		  noexcept override
		{
			return m_plane.normal();
		}

		virtual bool intersects(aabb const& box) const noexcept override
		{
			vector3 vs[2]{box.position(), box.position() + box.size()};

			// Some fancy algorithm! I'll just trust it does what it does.
			::std::size_t side1 = 0;
			::std::size_t side2 = 0;
			for (auto k : ::boost::irange(0, 2)) {
				for (auto j : ::boost::irange(0, 2)) {
					for (auto i : ::boost::irange(0, 2)) {
						vector3 point{vs[i].x(), vs[j].y(), vs[k].z()};

						if (point.dot(m_plane.normal()) + m_plane.depth() < 0)
							++side1;
						else
							++side2;
					}
				}
			}

			if (side1 == 0 || side2 == 0)
				return false;

			return true;
		}

		virtual aabb get_aabb() const noexcept override
		{
			return {vector3{-10000, -10000, -10000},
			        vector3{20000, 20000, 20000}};
		}
	};

	class box_primitive : public primitive
	{
	private:
		aabb m_box;

	public:
		box_primitive(aabb box) : m_box{box}
		{
		}

	private:
		virtual ::boost::optional<intersect_result>
		intersect_impl(ray const& ray, float const max_distance) const
		  noexcept override
		{
			float min_distance = ::std::numeric_limits<float>::infinity();
			float distances[6]{-1, -1, -1, -1, -1, -1};

			auto const& direction = ray.direction();
			auto const& origin = ray.origin();

			auto const& v1 = m_box.position();
			auto const& v2 = m_box.position() + m_box.size();

			if (direction.x() != 0) {
				distances[0] = (v1.x() - origin.x()) / direction.x();
				distances[3] = (v2.x() - origin.x()) / direction.x();
			}
			if (direction.y() != 0) {
				distances[1] = (v1.y() - origin.y()) / direction.y();
				distances[4] = (v2.y() - origin.y()) / direction.y();
			}
			if (direction.z() != 0) {
				distances[2] = (v1.z() - origin.z()) / direction.z();
				distances[5] = (v2.z() - origin.z()) / direction.z();
			}

			for (auto i : ::boost::irange(0, 6)) {
				// Ignore intersections behind the camera.
				if (distances[i] < 0)
					continue;

				auto v = origin + distances[i] * direction;

				if ((v.x() > v1.x() - epsilon) && (v.x() < v2.x() + epsilon) &&
				    (v.y() > v1.y() - epsilon) && (v.y() < v2.y() + epsilon) &&
				    (v.z() > v1.z() - epsilon) && (v.z() < v2.z() + epsilon)) {
					if (distances[i] < min_distance) {
						min_distance = distances[i];
					}
				}
			}

			if (min_distance >= max_distance)
				return {};

			if (this->m_box.contains(origin))
				return intersect_result{min_distance, inside::yes};

			return intersect_result{min_distance, inside::no};
		}

		virtual light_bundle get_lighting_points_impl() const override
		{
			static ::std::uniform_real_distribution<float> rand{
			  0, ::std::nextafter(1.f, ::std::numeric_limits<float>::max())};

			auto center = m_box.position() + m_box.size() / 2;

			constexpr ::std::size_t num_samples = 32;

			// The box will be split into ::std::pow(voxel_count, 3) voxels,
			// with one sample from each voxel.
			constexpr ::std::size_t voxel_count = 3;
			auto const voxel_size = m_box.size() / voxel_count;

			auto to_indices = [](::std::size_t sample) {
				auto i = sample % voxel_count;
				sample /= voxel_count;
				auto j = sample % voxel_count;
				sample /= voxel_count;
				auto k = sample % voxel_count;
				return vector3(i, j, k);
			};

			::std::vector<vector3> points;
			points.reserve(num_samples);

			for (auto i : ::boost::irange<::std::size_t>(0, num_samples)) {
				auto const indices = to_indices(i);
				auto const voxel_position =
				  m_box.position() + voxel_size * indices;
				vector3 const offset{rand(generator), rand(generator),
				                     rand(generator)};
				auto point = voxel_position + offset * voxel_size;
				points.push_back(point);
			}

			return {center, points};
		}

	public:
		virtual vector3 get_normal_at(vector3 const& position) const
		  noexcept override
		{
			auto diff1 = m_box.position() - position;
			auto diff2 = m_box.position() + m_box.size() - position;

			float const dist[6]{
			  ::std::abs(diff1.x()), ::std::abs(diff2.x()),
			  ::std::abs(diff1.y()), ::std::abs(diff2.y()),
			  ::std::abs(diff1.z()), ::std::abs(diff2.z())};
			::std::size_t best = 0;
			for (auto i : ::boost::irange<::std::size_t>(0, 6)) {
				if (dist[i] < dist[best]) {
					best = i;
				}
			}

			switch (best) {
			case 0:
				return vector3{-1, 0, 0};
			case 1:
				return vector3{1, 0, 0};
			case 2:
				return vector3{0, -1, 0};
			case 3:
				return vector3{0, 1, 0};
			case 4:
				return vector3{0, 0, -1};
			case 5:
			default:
				return vector3{0, 0, 1};
			}
		}

		virtual bool intersects(aabb const& box) const noexcept override
		{
			return m_box.intersect(box);
		}

		virtual aabb get_aabb() const noexcept override
		{
			return m_box;
		}
	};
}

#endif // PRIMITIVE_HPP
