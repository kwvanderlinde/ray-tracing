#ifdef PROFILING
#include <google/profiler.h>
#endif

#include "camera.hpp"
#include "color.hpp"
#include "common.hpp"
#include "engine.hpp"
#include "grid.hpp"
#include "hit.hpp"
#include "material.hpp"
#include "params.hpp"
#include "plane.hpp"
#include "primitive.hpp"
#include "ray.hpp"
#include "scene.hpp"
#include "surface.hpp"
#include "vector3.hpp"

#include <boost/range/irange.hpp>
#include <vlinde/memory/make_unique.hpp>

#include <fstream>
#include <type_traits>

raytracer::scene makeScene()
{
	using namespace raytracer;

	scene scene;

	{
		auto floor_plane = ::vlinde::memory::make_unique<plane_primitive>(
		  plane{vector3{0, 1, 0}, 4.4f});
		floor_plane->name("floor plane");
		floor_plane->material().reflection(0);
		floor_plane->material().refraction(0);
		floor_plane->material().diffuse(1);
		floor_plane->material().color({0.4f, 0.3f, 0.3f});
		scene.add(::std::move(floor_plane));
	}

	{
		auto back_plane = ::vlinde::memory::make_unique<plane_primitive>(
		  plane{vector3{0, 0, -1}, 12.4f});
		back_plane->name("back plane");
		back_plane->material().reflection(0);
		back_plane->material().refraction(0);
		back_plane->material().diffuse(1);
		back_plane->material().color({0.4f, 0.3f, 0.3f});
		scene.add(::std::move(back_plane));
	}

	{
		auto ceiling_plane = ::vlinde::memory::make_unique<plane_primitive>(
		  plane{vector3{0, -1, 0}, 10});
		ceiling_plane->name("ceiling plane");
		ceiling_plane->material().reflection(0);
		ceiling_plane->material().refraction(0);
		ceiling_plane->material().diffuse(1);
		ceiling_plane->material().color({0.4f, 0.3f, 0.3f});
		scene.add(::std::move(ceiling_plane));
	}

	{
		auto big_sphere = ::vlinde::memory::make_unique<sphere_primitive>(
		  vector3{0, -0.8f, 7}, 2);
		big_sphere->name("big sphere");
		big_sphere->material().reflection(0.2f);
		big_sphere->material().refraction(0.8f);
		big_sphere->material().refraction_index(1.3f);
		big_sphere->material().color({0.7f, 0.7f, 1.0f});
		big_sphere->material().diffuse_reflection(0.3f);
		scene.add(::std::move(big_sphere));
	}

	{
		auto small_sphere = ::vlinde::memory::make_unique<sphere_primitive>(
		  vector3{-5, -0.8f, 7}, 2);
		small_sphere->name("small sphere");
		small_sphere->material().reflection(0.5f);
		small_sphere->material().refraction(0);
		small_sphere->material().refraction_index(1.3f);
		small_sphere->material().diffuse(0.1f);
		small_sphere->material().color({0.7f, 0.7f, 1.0f});
		small_sphere->material().diffuse_reflection(0.6f);
		scene.add(::std::move(small_sphere));
	}

	{
		auto extra_sphere = ::vlinde::memory::make_unique<sphere_primitive>(
		  vector3{5, -0.8f, 7}, 2);
		extra_sphere->name("extra sphere");
		extra_sphere->material().reflection(0.5f);
		extra_sphere->material().refraction(0);
		extra_sphere->material().diffuse(0.1f);
		extra_sphere->material().color({0.7f, 0.7f, 1.0f});
		scene.add(::std::move(extra_sphere));
	}

	{
#if 1
		auto light1 = ::vlinde::memory::make_unique<box_primitive>(
		  aabb{vector3{-1, 5, 4}, vector3{2, 0.1f, 2}});
#else
		auto light1 = ::vlinde::memory::make_unique<sphere_primitive>(
		  vector3{0, 5, 5}, 0.1f);
#endif
		light1->is_light(true);
		light1->material().color({1, 1, 1});
		scene.add(::std::move(light1));
	}

	scene.build_grid();

	return scene;
}

int main()
{
#ifdef PROFILING
	ProfilerStart("profile/profile.out");
#endif

	using namespace raytracer;

	auto const width = 800;
	auto const height = 600;

	// Setup the rendering engine.
	engine engine;
	engine.trace_depth(6);
	engine.render_target(surface{width, height});
	engine.scene(makeScene());

	// render the scene.
	engine.render();

#ifdef PROFILING
	ProfilerStop();
#endif

	// write the image to a pbm file.
	::std::ofstream out{"test.ppm"};
	out << "P3" << ::std::endl << engine.render_target().width() << " "
	    << engine.render_target().height() << ::std::endl << "255"
	    << ::std::endl;

	for (auto const& color : engine.render_target()) {
		auto red = clamp(static_cast<int>(255.f * color.red()), 0, 255);
		auto green = clamp(static_cast<int>(255.f * color.green()), 0, 255);
		auto blue = clamp(static_cast<int>(255.f * color.blue()), 0, 255);

		out << red << " " << green << " " << blue << " ";
	}

	return 0;
}
