#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <array>
#include <cmath>

#include <boost/range/algorithm.hpp>
#include <boost/range/numeric.hpp>

namespace raytracer
{
	class vector3
	{
	private:
		std::array<float, 3> m_impl;

	public:
		vector3() noexcept : m_impl{}
		{
		}

		vector3(float x, float y, float z) noexcept : m_impl{{x, y, z}}
		{
		}

		float x() const noexcept
		{
			return ::std::get<0>(m_impl);
		}

		void x(float x) noexcept
		{
			::std::get<0>(m_impl) = x;
		}

		float y() const noexcept
		{
			return ::std::get<1>(m_impl);
		}

		void y(float y) noexcept
		{
			::std::get<1>(m_impl) = y;
		}

		float z() const noexcept
		{
			return ::std::get<2>(m_impl);
		}

		void z(float z) noexcept
		{
			::std::get<2>(m_impl) = z;
		}

		vector3 & normalize() noexcept
		{
			float const length = this->length();
			for (auto& x : m_impl)
				x /= length;
      return *this;
		}

		float length() const noexcept
		{
			return ::std::sqrt(this->square_length());
		}

		float square_length() const noexcept
		{
			return this->dot(*this);
		}

		float dot(vector3 const& other) const noexcept
		{
			return ::boost::inner_product(m_impl, other.m_impl, 0.f);
		}

		vector3 cross(vector3 const& other) const noexcept
		{
			float x =
			  this->y() * other.z() - this->z() * other.y();
			float y =
			  this->z() * other.x() - this->x() * other.z();
			float z =
			  this->x() * other.y() - this->y() * other.x();

			return {x, y, z};
		}

		vector3& operator+=(vector3 const& other) noexcept
		{
			::boost::transform(m_impl, other.m_impl, m_impl.begin(),
			                   ::std::plus<float>{});
			return *this;
		}

		vector3& operator-=(vector3 const& other) noexcept
		{
			::boost::transform(m_impl, other.m_impl, m_impl.begin(),
			                   ::std::minus<float>{});
			return *this;
		}

		vector3& operator*=(float f) noexcept
		{
			for (auto& x : m_impl)
				x *= f;
			return *this;
		}

		vector3& operator*=(vector3 const& other) noexcept
		{
			::boost::transform(m_impl, other.m_impl, m_impl.begin(),
			                   ::std::multiplies<float>{});
			return *this;
		}

		vector3& operator/=(float f) noexcept
		{
			for (auto& x : m_impl)
				x /= f;
			return *this;
		}

		vector3& operator/=(vector3 const& other) noexcept
		{
			::boost::transform(m_impl, other.m_impl, m_impl.begin(),
			                   ::std::divides<float>{});
			return *this;
		}

		vector3 operator-() const noexcept
		{
			vector3 result;
			::boost::transform(m_impl, result.m_impl.begin(),
			                   ::std::negate<float>{});
			return result;
		}

		friend vector3 operator+(vector3 const& a, vector3 const& b) noexcept
		{
			vector3 result{a};
			result += b;
			return result;
		}

		friend vector3 operator-(vector3 const& a, vector3 const& b) noexcept
		{
			vector3 result{a};
			result -= b;
			return result;
		}

		friend vector3 operator*(vector3 const& a, float f) noexcept
		{
      vector3 result{a};
      result *= f;
      return result;
		}

		friend vector3 operator*(vector3 const& a, vector3 const& b) noexcept
		{
			vector3 result{a};
			result *= b;
			return result;
		}

		friend vector3 operator*(float f, vector3 const& a) noexcept
		{
      vector3 result{a};
      result *= f;
      return result;
		}

		friend vector3 operator/(vector3 const& a, float f) noexcept
		{
      vector3 result{a};
      result /= f;
      return result;
		}

		friend vector3 operator/(vector3 const& a, vector3 const& b) noexcept
		{
			vector3 result{a};
			result /= b;
			return result;
		}
	};
}

#endif // VECTOR_HPP
