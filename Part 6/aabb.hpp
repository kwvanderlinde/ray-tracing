#ifndef AABB_HPP
#define AABB_HPP

#include "common.hpp"
#include "vector3.hpp"

namespace raytracer
{
  class ray;

	class aabb
	{
	private:
		vector3 m_position;
		vector3 m_size;

	public:
		aabb();

		aabb(vector3 const& position, vector3 const& size);

		vector3 const& position() const noexcept;

		vector3 const& size() const noexcept;

		bool intersect(aabb const& b2) const noexcept;

		bool contains(vector3 const& v) const noexcept;

		void advance_ray(ray& ray) const noexcept;
	};
}

#endif // AABB_HPP
