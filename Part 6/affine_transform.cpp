#include "affine_transform.hpp"

#include <utility>

namespace raytracer
{

	void affine_transform::transpose_rows()
	{
		using ::std::swap;
		swap(::std::get<1>(m_rows).x, ::std::get<0>(m_rows).y);
		swap(::std::get<2>(m_rows).x, ::std::get<0>(m_rows).z);
		swap(::std::get<2>(m_rows).y, ::std::get<1>(m_rows).z);
	}

	affine_transform::affine_transform(vector3 const& a, vector3 const& b,
	                                   vector3 const& c, vector3 const& t)
	  : m_rows{{a, b, c}}, m_translation{t}
	{
	}

	/**
	 * The identity transform.
	 */
	affine_transform::affine_transform() noexcept
	  : m_rows{{vector3::unit_x, vector3::unit_y, vector3::unit_z}},
	    m_translation{vector3::zero}
	{
	}

	vector3 affine_transform::operator()(vector3 const& v) const noexcept
	{
		return m_translation + vector3{v.dot(::std::get<0>(m_rows)),
		                               v.dot(::std::get<1>(m_rows)),
		                               v.dot(::std::get<2>(m_rows))};
	}

	// Some builder functions.

	affine_transform affine_transform::identity() noexcept
	{
		return {};
	}

	affine_transform
	affine_transform::lookat(vector3 const& eye, vector3 const& target,
	                         vector3 const& up) noexcept
	{
		auto z = (target - eye).normalize();
		auto x = up.cross(z).normalize();
		auto y = z.cross(x);

		affine_transform result{x, y, z, eye};
		result.transpose_rows();
		return result;
	}
}
