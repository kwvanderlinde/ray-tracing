#ifndef AFFINE_TRANSFORM_HPP
#define AFFINE_TRANSFORM_HPP

#include "vector3.hpp"

#include <array>

namespace raytracer
{
	class affine_transform
	{
	private:
		/* We represent affine part of the transform as a 3x3 matrix which
		 * acts on column vectors.
		 */

		::std::array<vector3, 3> m_rows;
		vector3 m_translation;

		void transpose_rows();

		affine_transform(vector3 const& a, vector3 const& b,
		                 vector3 const& c, vector3 const& t);

	public:
		/**
		 * The identity transform.
		 */
		affine_transform() noexcept;

		vector3 operator()(vector3 const& v) const noexcept;

		// Some builder functions.

		static affine_transform identity() noexcept;

		static affine_transform lookat(vector3 const& eye,
		                               vector3 const& target,
		                               vector3 const& up) noexcept;
	};
}

#endif // AFFINE_TRANSFORM_HPP
