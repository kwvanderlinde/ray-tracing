#include "camera.hpp"

#include <cmath>

namespace raytracer
{
	camera::camera(vector3 const& position, vector3 const& lookat,
	               vector3 const& up, real fov_y, real aspect)
	  : m_position{position}, m_lookat{lookat}, m_up{vector3{up}},
	    m_forward{lookat - position}, m_left{up.cross(m_forward)},
	    m_fov_y{fov_y}, m_aspect{aspect}, m_distance{m_forward.length()},
	    m_delta_y{m_distance * ::std::tan(m_fov_y / 2)},
	    m_delta_x{m_delta_y * m_aspect}
	{
		// normalize all our directional vectors.
		m_up.normalize();
		m_forward.normalize();
		m_left.normalize();
	}

	vector3 camera::getUpperLeft() const noexcept
	{
		return m_lookat + m_left * m_delta_x + m_up * m_delta_y;
	}
}
