#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "vector3.hpp"

namespace raytracer
{
	class camera
	{
	private:
		vector3 m_position;
		vector3 m_lookat;
		vector3 m_up;
		vector3 m_forward;
		vector3 m_left;

		real m_fov_y;
		real m_aspect;
		real m_distance;

		// The world size of half the viewing area.
		real m_delta_y;
		real m_delta_x;

	public:
		camera(vector3 const& position, vector3 const& lookat,
		       vector3 const& up, real fov_y, real aspect);

		vector3 getUpperLeft() const noexcept;
	};
}

#endif // CAMERA_HPP
