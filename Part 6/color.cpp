#include "color.hpp"

namespace raytracer
{
	color const color::black{0, 0, 0};
	color const color::white{1, 1, 1};

	color::color() noexcept : color{0, 0, 0}
	{
	}

	color::color(real r_, real g_, real b_) noexcept : r{r_}, g{g_}, b{b_}
	{
	}

	color& color::operator+=(color const& other) noexcept
	{
		r += other.r;
		g += other.g;
		b += other.b;

		return *this;
	}

	color& color::operator-=(color const& other) noexcept
	{
		r -= other.r;
		g -= other.g;
		b -= other.b;

		return *this;
	}

	color& color::operator*=(real f) noexcept
	{
		r *= f;
		g *= f;
		b *= f;

		return *this;
	}

	color& color::operator*=(color const& other) noexcept
	{
		r *= other.r;
		g *= other.g;
		b *= other.b;

		return *this;
	}

	color& color::operator/=(real f) noexcept
	{
		r /= f;
		g /= f;
		b /= f;

		return *this;
	}

	color& color::operator/=(color const& other) noexcept
	{
		r /= other.r;
		g /= other.g;
		b /= other.b;

		return *this;
	}

	color color::operator-() const noexcept
	{
		return {-r, -g, -b};
	}

	color operator+(color const& a, color const& b) noexcept
	{
		color result{a};
		result += b;
		return result;
	}

	color operator-(color const& a, color const& b) noexcept
	{
		color result{a};
		result -= b;
		return result;
	}

	color operator*(color const& a, real f) noexcept
	{
		color result{a};
		result *= f;
		return result;
	}

	color operator*(color const& a, color const& b) noexcept
	{
		color result{a};
		result *= b;
		return result;
	}

	color operator*(real f, color const& a) noexcept
	{
		color result{a};
		result *= f;
		return result;
	}

	color operator/(color const& a, real f) noexcept
	{
		color result{a};
		result /= f;
		return result;
	}

	color operator/(color const& a, color const& b) noexcept
	{
		color result{a};
		result /= b;
		return result;
	}

	color operator/(real f, color const& a) noexcept
	{
		color result{a};
		result /= f;
		return result;
	}
}
