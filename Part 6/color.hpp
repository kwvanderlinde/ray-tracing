#ifndef COLOR_HPP
#define COLOR_HPP

#include "common.hpp"

namespace raytracer
{
	class color
	{
	public:
		static color const black;
		static color const white;

		real r;
		real g;
		real b;

		color() noexcept;

		color(real r_, real g_, real b_) noexcept;

		color& operator+=(color const& other) noexcept;

		color& operator-=(color const& other) noexcept;

		color& operator*=(real f) noexcept;

		color& operator*=(color const& other) noexcept;

		color& operator/=(real f) noexcept;

		color& operator/=(color const& other) noexcept;

		color operator-() const noexcept;
	};

	color operator+(color const& a, color const& b) noexcept;

	color operator-(color const& a, color const& b) noexcept;

	color operator*(color const& a, real f) noexcept;

	color operator*(color const& a, color const& b) noexcept;

	color operator*(real f, color const& a) noexcept;

	color operator/(color const& a, real f) noexcept;

	color operator/(color const& a, color const& b) noexcept;

	color operator/(real f, color const& a) noexcept;
}

#endif // COLOR_HPP
