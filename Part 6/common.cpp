#include "common.hpp"

static ::std::random_device device{};
::std::mt19937_64 generator{device()};

constexpr ::std::size_t grid_size = 8;
constexpr real epsilon = 0.001f;
constexpr real pi = 3.141592653589793238462f;

auto& _ = ::std::ignore;
