#ifndef COMMON_HPP
#define COMMON_HPP

#include <cstddef>
#include <random>
#include <tuple>

using real = double;

extern ::std::mt19937_64 generator;

extern const ::std::size_t grid_size;
extern const real epsilon;
extern const real pi;

extern decltype(::std::ignore)& _;

template <typename T>
constexpr T const& clamp(T const& a, T const& min, T const& max)
{
	return (a < min) ? min : ((a > max) ? max : a);
}

#endif // COMMON_HPP
