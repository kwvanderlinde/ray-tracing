#include "engine.hpp"

#include "affine_transform.hpp"
#include "renderer.hpp"
#include "scene.hpp"
#include "surface.hpp"
#include "vector3.hpp"

#include <cstddef>

namespace raytracer
{
	engine::engine()
	  : m_target{vector3::zero}, m_eye{-5 * vector3::unit_z},
	    m_up{vector3::unit_y}, m_scene{}, m_trace_depth{1}, m_samples{1}
	{
	}

	vector3 engine::target() const noexcept
	{
		return m_target;
	}

	void engine::target(vector3 target) noexcept
	{
		m_target = target;
	}

	vector3 engine::eye() const noexcept
	{
		return m_eye;
	}

	void engine::eye(vector3 eye) noexcept
	{
		m_eye = eye;
	}

	vector3 engine::up() const noexcept
	{
		return m_up;
	}

	void engine::up(vector3 up) noexcept
	{
		m_up = up;
	}

	class scene& engine::scene() noexcept
	{
		return m_scene;
	}

	class scene const& engine::scene() const noexcept
	{
		return m_scene;
	}

	void engine::scene(class scene&& scene)
	{
		m_scene = ::std::move(scene);
	}

	::std::size_t engine::trace_depth() const noexcept
	{
		return m_trace_depth;
	}

	void engine::trace_depth(::std::size_t trace_depth) noexcept
	{
		m_trace_depth = trace_depth;
	}

	::std::size_t engine::samples() const noexcept
	{
		return m_samples;
	}

	void engine::samples(::std::size_t const& samples) noexcept
	{
		m_samples = samples;
	}

	void engine::render_to(surface& render_target) const
	{
		renderer const renderer{m_scene, m_trace_depth, m_samples};

		renderer.render(affine_transform::lookat(m_eye, m_target, m_up),
		                render_target);
	}
}
