#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "scene.hpp"
#include "vector3.hpp"

#include <cstddef>

namespace raytracer
{
  class surface;

	class engine
	{
	private:
		vector3 m_target;
		vector3 m_eye;
		vector3 m_up;
		scene m_scene;
		::std::size_t m_trace_depth;
		::std::size_t m_samples;

	public:
		engine();

		vector3 target() const noexcept;

		void target(vector3 target) noexcept;

		vector3 eye() const noexcept;

		void eye(vector3 eye) noexcept;

		vector3 up() const noexcept;

		void up(vector3 up) noexcept;

		class scene& scene() noexcept;

		class scene const& scene() const noexcept;

		void scene(class scene&& scene);

		::std::size_t trace_depth() const noexcept;

		void trace_depth(::std::size_t trace_depth) noexcept;

		::std::size_t samples() const noexcept;

		void samples(::std::size_t const& samples) noexcept;

		void render_to(surface& render_target) const;
	};
}

#endif // ENGINE_HPP
