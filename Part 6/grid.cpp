#include "grid.hpp"

#include "aabb.hpp"
#include "vector3.hpp"

#include <vlinde/memory/make_unique.hpp>

#include <cassert>
#include <cstddef>

namespace raytracer
{
	grid::grid(aabb extents, ::std::size_t size_x, ::std::size_t size_y,
	           ::std::size_t size_z)
	  : m_extents{extents}, m_size_x{size_x}, m_size_y{size_y},
	    m_size_z{size_z}, m_size{m_size_x * m_size_y * m_size_z},
	    m_impl{ ::vlinde::memory::make_unique<cell[]>(m_size)}
	{
		vector3 const voxel_counts(m_size_x, m_size_y, m_size_z);
		m_cell_size = m_extents.size() / voxel_counts;

		for (::std::size_t z = 0; z < m_size_z; ++z) {
			for (::std::size_t y = 0; y < m_size_y; ++y) {
				for (::std::size_t x = 0; x < m_size_x; ++x) {
					vector3 const voxel(x, y, z);
					auto const pos = m_extents.position() + m_cell_size * voxel;
					(*this)(x, y, z).extents = {pos, m_cell_size};
				}
			}
		}
	}

	aabb const& grid::extents() const noexcept
	{
		return m_extents;
	}

	::std::size_t grid::size_x() const noexcept
	{
		return m_size_x;
	}

	::std::size_t grid::size_y() const noexcept
	{
		return m_size_y;
	}

	::std::size_t grid::size_z() const noexcept
	{
		return m_size_z;
	}

	vector3 const& grid::cell_size() const noexcept
	{
		return m_cell_size;
	}

	cell& grid::operator()(size_type x, size_type y, size_type z) noexcept
	{
		assert(x < m_size_x);
		assert(y < m_size_y);
		assert(z < m_size_z);

		return m_impl[x + y * m_size_x + z * m_size_x * m_size_y];
	}

	cell const& grid::operator()(size_type x, size_type y,
	                             size_type z) const noexcept
	{
		assert(x < m_size_x);
		assert(y < m_size_y);
		assert(z < m_size_z);

		return m_impl[x + y * m_size_x + z * m_size_x * m_size_y];
	}
}
