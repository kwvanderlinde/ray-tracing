#ifndef GRID_HPP
#define GRID_HPP

#include "aabb.hpp"

#include <cstddef>
#include <memory>
#include <vector>

namespace raytracer
{
  class primitive;
  
	struct cell
	{
		aabb extents;
		::std::vector<primitive const*> primitives;
	};

	class grid
	{
	private:
		aabb m_extents;
		vector3 m_cell_size;
		::std::size_t m_size_x; // x-direction
		::std::size_t m_size_y; // y-direction
		::std::size_t m_size_z; // z-direction
		::std::size_t m_size;
		::std::unique_ptr<cell[]> m_impl;

	public:
		using size_type = ::std::size_t;

		grid(aabb extents, ::std::size_t size_x, ::std::size_t size_y,
		     ::std::size_t size_z);

		aabb const& extents() const noexcept;

		::std::size_t size_x() const noexcept;

		::std::size_t size_y() const noexcept;

		::std::size_t size_z() const noexcept;

		vector3 const& cell_size() const noexcept;

		cell& operator()(size_type x, size_type y, size_type z) noexcept;

		cell const& operator()(size_type x, size_type y, size_type z) const
		  noexcept;
	};
}

#endif // GRID_HPP
