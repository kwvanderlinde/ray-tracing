#ifndef HIT_RESULT_HPP
#define HIT_RESULT_HPP

#include "common.hpp"

namespace raytracer
{
  class primitive;

	enum class inside : bool {
		no = false,
		yes = true
	};

	struct hit
	{
    primitive const* primitive;
		real distance;
		inside inside;
	};
}

#endif // HIT_RESULT_HPP
