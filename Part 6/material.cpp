#include "material.hpp"

#include "color.hpp"
#include "texture.hpp"

#include <boost/optional.hpp>

namespace raytracer
{
  material::material() noexcept : m_color{0.2f, 0.2f, 0.2f},
		                      m_diff{0.2f},
		                      m_spec{0.8f},
		                      m_spec_exp{20.f},
		                      m_refl{0},
		                      m_diff_refl{0},
		                      m_refr{0},
		                      m_rindex{1.5f},
		                      m_texture{},
		                      m_u_scale{1.f},
		                      m_v_scale{1.f}
		{
		}

		class color& material::color() noexcept
		{
			return m_color;
		}

		class color const& material::color() const noexcept
		{
			return m_color;
		}

		void material::color(class color const& color) noexcept
		{
			m_color = color;
		}

		real material::diffuse() const noexcept
		{
			return m_diff;
		}

		void material::diffuse(real diff) noexcept
		{
			m_diff = diff;
		}

		real material::reflection() const noexcept
		{
			return m_refl;
		}

		void material::reflection(real refl) noexcept
		{
			m_refl = refl;
		}

		real material::diffuse_reflection() const noexcept
		{
			return m_diff_refl;
		}

		void material::diffuse_reflection(real diff_refl) noexcept
		{
			m_diff_refl = diff_refl;
		}

		real material::refraction() const noexcept
		{
			return m_refr;
		}

		void material::refraction(real refr) noexcept
		{
			m_refr = refr;
		}

		real material::refraction_index() const noexcept
		{
			return m_rindex;
		}

		void material::refraction_index(real index) noexcept
		{
			m_rindex = index;
		}

		real material::specular() const noexcept
		{
			return m_spec;
		}

		void material::specular(real spec) noexcept
		{
			m_spec = spec;
		}

		real material::specular_exponent() const noexcept
		{
			return m_spec_exp;
		}

		void material::specular_exponent(real spec) noexcept
		{
			m_spec_exp = spec;
		}

		::boost::optional<texture> const& material::texture() const noexcept
		{
			return m_texture;
		}

		::boost::optional<class texture>& material::texture() noexcept
		{
			return m_texture;
		}

		void
		material::texture(::boost::optional<class texture> const& texture) noexcept
		{
			m_texture = texture;
		}

		real material::u_scale() const noexcept
		{
			return m_u_scale;
		}

		void material::u_scale(real scale) noexcept
		{
			m_u_scale = scale;
		}

		real material::v_scale() const noexcept
		{
			return m_v_scale;
		}

		void material::v_scale(real scale) noexcept
		{
			m_v_scale = scale;
		}
}
