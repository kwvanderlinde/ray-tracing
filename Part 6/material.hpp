#ifndef MATERIAL_HPP
#define MATERIAL_HPP

#include "color.hpp"
#include "texture.hpp"

#include <boost/optional.hpp>

namespace raytracer
{
	// TODO Need to a way to signal the lack of a texture. Probably use
	// boost::optional.
	class material
	{
	private:
		color m_color;
		real m_diff;
		real m_spec;
		real m_spec_exp;
		real m_refl;
		real m_diff_refl;
		real m_refr;
		real m_rindex;
		::boost::optional<texture> m_texture;
		real m_u_scale;
		real m_v_scale;

	public:
		material() noexcept;

		class color& color() noexcept;

		class color const& color() const noexcept;

		void color(class color const& color) noexcept;

		real diffuse() const noexcept;

		void diffuse(real diff) noexcept;

		real reflection() const noexcept;

		void reflection(real refl) noexcept;

		real diffuse_reflection() const noexcept;

		void diffuse_reflection(real diff_refl) noexcept;

		real refraction() const noexcept;

		void refraction(real refr) noexcept;

		real refraction_index() const noexcept;

		void refraction_index(real index) noexcept;

		real specular() const noexcept;

		void specular(real spec) noexcept;

		real specular_exponent() const noexcept;

		void specular_exponent(real spec) noexcept;

		::boost::optional<texture> const& texture() const noexcept;

		::boost::optional<class texture>& texture() noexcept;

		void
		texture(::boost::optional<class texture> const& texture) noexcept;

		real u_scale() const noexcept;

		void u_scale(real scale) noexcept;

		real v_scale() const noexcept;

		void v_scale(real scale) noexcept;
	};
}

#endif // MATERIAL_HPP
