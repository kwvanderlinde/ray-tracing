#include "primitive.hpp"

#include "aabb.hpp"
#include "color.hpp"
#include "hit.hpp"
#include "material.hpp"
#include "ray.hpp"
#include "texture.hpp"
#include "vector3.hpp"

#include <boost/optional.hpp>
#include <vlinde/memory/make_unique.hpp>

#include <cmath>
#include <cstddef>
#include <limits>
#include <memory>
#include <random>
#include <string>
#include <vector>

namespace raytracer
{
	primitive::primitive() : m_material{}, m_name{""}, m_is_light{false}
	{
	}

	primitive::~primitive() = default;

	class material& primitive::material() noexcept
	{
		return m_material;
	}

	class material const& primitive::material() const noexcept
	{
		return m_material;
	}

	void primitive::material(class material const& material) noexcept
	{
		m_material = material;
	}

	::std::string const& primitive::name() const
	{
		return m_name;
	}

	void primitive::name(std::string const& name)
	{
		m_name = name;
	}

	bool primitive::is_light() const noexcept
	{
		return this->get_is_light();
	}

	void primitive::is_light(bool is_light) noexcept
	{
		this->set_is_light(is_light);
	}

	bool primitive::get_is_light() const noexcept
	{
		return m_is_light;
	}

	void primitive::set_is_light(bool is_light) noexcept
	{
		m_is_light = is_light;
	}

	light_bundle primitive::get_lighting_points_impl(::std::size_t) const
	{
		return {vector3::zero, {}};
	}

	light_bundle
	primitive::get_lighting_points(::std::size_t const num_samples) const
	{
		if (!this->is_light())
			return {vector3::zero, {}};

		return this->get_lighting_points_impl(num_samples);
	}

	::boost::optional<hit> primitive::intersect(ray const& ray,
	                                            real dist) const noexcept
	{
		auto result = intersect_impl(ray, dist);
		if (!result)
			return {};

		return hit{this, result->distance, result->inside};
	}

	color primitive::get_color_at_impl(vector3 const&,
	                                   texture const&) const noexcept
	{
		return m_material.color();
	}

	color primitive::get_color_at(vector3 const& position) const noexcept
	{
		if (auto const& texture = this->material().texture())
			return get_color_at_impl(position, *texture);

		return m_material.color();
	}

	namespace
	{
		class sphere_primitive : public primitive
		{
		private:
			vector3 m_center;
			real m_radius;

			// To avoid recomputations
			real m_square_radius;

		public:
			sphere_primitive(vector3 const& center, real radius)
			  : m_center{center}, m_radius{radius},
			    m_square_radius{radius * radius}
			{
			}

		private:
			virtual light_bundle get_lighting_points_impl(::std::size_t) const
			  override
			{
				return {m_center, {}};
			}

			virtual color get_color_at_impl(vector3 const& position,
			                                texture const& texture) const
			  noexcept override
			{
				// TODO: Allow replacing `vector3::unit_*` with an arbitrary
				// orthonormal system. Currently, I can just use coordinates for
				// simpler code.

				auto const vp = this->get_normal_at(position);
				auto const phi = ::std::acos(vp.dot(vector3::unit_y));
				auto const v = phi / pi * this->material().v_scale();
				auto const theta =
				  ::std::acos(vp.dot(vector3::unit_x) / ::std::sin(phi)) / 2.f /
				  pi;
				auto const base_u =
				  vp.dot(vector3::unit_z) >= 0 ? (1 - theta) : theta;
				auto const u = base_u * this->material().u_scale();

				return texture(u, v) * this->material().color();
			}

		public:
			virtual ::boost::optional<intersect_result>
			intersect_impl(ray const& ray, real const dist) const
			  noexcept override
			{
				auto const v = ray.origin() - m_center;
				auto const b = -v.dot(ray.direction());
				auto det = (b * b) - v.square_length() + m_square_radius;

				if (det < 0)
					return {};

				det = ::std::sqrt(det);
				auto const i1 = b - det;
				auto const i2 = b + det;

				if (i2 < 0)
					return {};

				if (i1 >= dist)
					return {};

				if (i1 >= 0) {
					return intersect_result{i1, inside::no};
				}

				if (i2 < dist) {
					return intersect_result{i2, inside::yes};
				}

				return {};
			}

			virtual vector3 get_normal_at(vector3 const& position) const
			  noexcept override
			{
				return (position - m_center).normalize();
			}

			virtual bool intersects(aabb const& box) const noexcept override
			{
				real dmin = 0;
				auto const& v1 = box.position();
				auto v2 = box.position() + box.size();

				{
					real diff = 0;
					if (m_center.x < v1.x)
						diff = m_center.x - v1.x;
					else if (m_center.x > v2.x)
						diff = m_center.x - v2.x;
					dmin += diff * diff;
				}

				{
					real diff = 0;
					if (m_center.y < v1.y)
						diff = m_center.y - v1.y;
					else if (m_center.y > v2.y)
						diff = m_center.y - v2.y;
					dmin += diff * diff;
				}

				{
					real diff = 0;
					if (m_center.z < v1.z)
						diff = m_center.z - v1.z;
					else if (m_center.z > v2.z)
						diff = m_center.z - v2.z;
					dmin += diff * diff;
				}

				return dmin <= m_square_radius;
			}

			virtual aabb get_aabb() const noexcept override
			{
				vector3 size{m_radius, m_radius, m_radius};
				return {m_center - size, size * 2};
			}
		};

		class plane_primitive : public primitive
		{
		private:
			vector3 m_normal;
			real m_depth;
			vector3 m_u_axis;
			vector3 m_v_axis;

		public:
			plane_primitive(vector3 const& normal, real depth)
			  : m_normal{normal}, m_depth{depth},
			    m_u_axis{normal.y, normal.z,
			             -normal.x},
			    m_v_axis{m_u_axis.cross(normal).normalize()}
			{
			}

		private:
			virtual bool get_is_light() const noexcept override
			{
				return false;
			}

			virtual ::boost::optional<intersect_result>
			intersect_impl(ray const& ray, real const dist) const
			  noexcept override
			{
				auto const d = m_normal.dot(ray.direction());
				if (d == 0)
					return {};

				auto const normal_projection = m_normal.dot(ray.origin());
				auto const distance = -(normal_projection + m_depth) / d;

				if (distance < 0 || distance >= dist)
					return {};

				return intersect_result{distance, inside::no};
			}

			virtual color get_color_at_impl(vector3 const& position,
			                                texture const& texture) const
			  noexcept override
			{
				// We actually have a texture.
				auto const u =
				  position.dot(m_u_axis) * this->material().u_scale();
				auto const v =
				  position.dot(m_v_axis) * this->material().v_scale();
				return texture(u, v) * this->material().color();
			}

		public:
			virtual vector3 get_normal_at(vector3 const&) const
			  noexcept override
			{
				return m_normal;
			}

			virtual bool intersects(aabb const& box) const noexcept override
			{
				vector3 vs[2]{box.position(), box.position() + box.size()};

				// Some fancy algorithm! I'll just trust it does what it does.
				::std::size_t side1 = 0;
				::std::size_t side2 = 0;
				for (auto k = 0; k < 2; ++k) {
					for (auto j = 0; j < 2; ++j) {
						for (auto i = 0; i < 2; ++i) {
							vector3 point{vs[i].x, vs[j].y, vs[k].z};

							if (point.dot(m_normal) + m_depth < 0)
								++side1;
							else
								++side2;
						}
					}
				}

				if (side1 == 0 || side2 == 0)
					return false;

				return true;
			}

			virtual aabb get_aabb() const noexcept override
			{
				return {vector3{-10000, -10000, -10000},
				        vector3{20000, 20000, 20000}};
			}
		};

		class box_primitive : public primitive
		{
		private:
			aabb m_box;

		public:
			box_primitive(aabb box) : m_box{box}
			{
			}

		private:
			virtual ::boost::optional<intersect_result>
			intersect_impl(ray const& ray, real const max_distance) const
			  noexcept override
			{
				real min_distance = ::std::numeric_limits<real>::infinity();
				real distances[6]{-1, -1, -1, -1, -1, -1};

				auto const& direction = ray.direction();
				auto const& origin = ray.origin();

				auto const& v1 = m_box.position();
				auto const& v2 = m_box.position() + m_box.size();

				if (direction.x != 0) {
					distances[0] = (v1.x - origin.x) / direction.x;
					distances[3] = (v2.x - origin.x) / direction.x;
				}
				if (direction.y != 0) {
					distances[1] = (v1.y - origin.y) / direction.y;
					distances[4] = (v2.y - origin.y) / direction.y;
				}
				if (direction.z != 0) {
					distances[2] = (v1.z - origin.z) / direction.z;
					distances[5] = (v2.z - origin.z) / direction.z;
				}

				for (auto i = 0; i < 6; ++i) {
					// Ignore intersections behind the camera.
					if (distances[i] < 0)
						continue;

					auto v = origin + distances[i] * direction;

					if ((v.x > v1.x - epsilon) && (v.x < v2.x + epsilon) &&
					    (v.y > v1.y - epsilon) && (v.y < v2.y + epsilon) &&
					    (v.z > v1.z - epsilon) && (v.z < v2.z + epsilon)) {
						if (distances[i] < min_distance) {
							min_distance = distances[i];
						}
					}
				}

				if (min_distance >= max_distance)
					return {};

				if (this->m_box.contains(origin))
					return intersect_result{min_distance, inside::yes};

				return intersect_result{min_distance, inside::no};
			}

			virtual light_bundle
			get_lighting_points_impl(::std::size_t const num_samples) const
			  override
			{
				static ::std::uniform_real_distribution<real> rand{
				  0,
				  ::std::nextafter(1.f, ::std::numeric_limits<real>::max())};

				auto center = m_box.position() + m_box.size() / 2;

				// The box will be split into ::std::pow(voxel_count, 3) voxels,
				// with one sample from each voxel.
				constexpr ::std::size_t voxel_count = 3;
				auto const voxel_size = m_box.size() / voxel_count;

				auto to_indices = [](::std::size_t sample) {
					auto i = sample % voxel_count;
					sample /= voxel_count;
					auto j = sample % voxel_count;
					sample /= voxel_count;
					auto k = sample % voxel_count;
					return vector3(i, j, k);
				};

				::std::vector<vector3> points;
				points.reserve(num_samples);

				for (::std::size_t i = 0; i < num_samples; ++i) {
					auto const indices = to_indices(i);
					auto const voxel_position =
					  m_box.position() + voxel_size * indices;
					vector3 const offset{rand(generator), rand(generator),
					                     rand(generator)};
					auto point = voxel_position + offset * voxel_size;
					points.push_back(point);
				}

				return {center, points};
			}

		public:
			virtual vector3 get_normal_at(vector3 const& position) const
			  noexcept override
			{
				auto diff1 = m_box.position() - position;
				auto diff2 = m_box.position() + m_box.size() - position;

				real const dist[6]{ ::std::abs(diff1.x), ::std::abs(diff2.x),
				                     ::std::abs(diff1.y), ::std::abs(diff2.y),
				                     ::std::abs(diff1.z), ::std::abs(diff2.z)};
				::std::size_t best = 0;
				for (::std::size_t i = 0; i < 6; ++i) {
					if (dist[i] < dist[best]) {
						best = i;
					}
				}

				switch (best) {
				case 0:
					return -vector3::unit_x;
				case 1:
					return vector3::unit_x;
				case 2:
					return -vector3::unit_y;
				case 3:
					return vector3::unit_y;
				case 4:
					return -vector3::unit_z;
				case 5:
				default:
					return vector3::unit_z;
				}
			}

			virtual bool intersects(aabb const& box) const noexcept override
			{
				return m_box.intersect(box);
			}

			virtual aabb get_aabb() const noexcept override
			{
				return m_box;
			}
		};
	}

	::std::unique_ptr<primitive> make_sphere(vector3 const& center,
	                                         real radius)
	{
		return ::vlinde::memory::make_unique<sphere_primitive>(center,
		                                                       radius);
	}

	::std::unique_ptr<primitive> make_plane(vector3 const& normal,
	                                        real depth)
	{
		return ::vlinde::memory::make_unique<plane_primitive>(normal,
		                                                      depth);
	}

	::std::unique_ptr<primitive> make_box(aabb const& box)
	{
		return ::vlinde::memory::make_unique<box_primitive>(box);
	}
}
