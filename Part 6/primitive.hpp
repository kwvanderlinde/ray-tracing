#ifndef PRIMITIVE_HPP
#define PRIMITIVE_HPP

#include "hit.hpp"
#include "material.hpp"
#include "vector3.hpp"

#include <boost/optional/optional_fwd.hpp>

#include <cstddef>
#include <string>
#include <vector>

namespace raytracer
{
  class aabb;
  class ray;

	struct intersect_result
	{
		real distance;
		inside inside;
	};

	/**
	 * Represents lighting information for use in the ray tracing engine.
	 */
	struct light_bundle
	{
		/**
		 * A central representative of the light source.
		 *
		 * This is used to implement non-sampling lighting models such as
		 * the Phong model.
		 */
		vector3 center;

		/**
		 * A sampling of points of the light source.
		 *
		 * This is used to calculate soft shadows. If empty, hard shadows
		 * are calculated using `center` as the sole sample. Otherwise, soft
		 * shadows are calculated via the proportion of occluded samples.
		 */
		::std::vector<vector3> samples;
	};

	class primitive
	{
	private:
		material m_material;
		::std::string m_name;
		bool m_is_light;

	public:
		primitive();

		virtual ~primitive();

		class material& material() noexcept;

		class material const& material() const noexcept;

		void material(class material const& material) noexcept;

		::std::string const& name() const;

		void name(std::string const& name);

		bool is_light() const noexcept;

		void is_light(bool is_light) noexcept;

	private:
		/**
		 * Overridable implementation of the `is_light` getter.
		 */
		virtual bool get_is_light() const noexcept;

		/**
		 * Overridable implementation of the `is_light` setter.
		 */
		virtual void set_is_light(bool is_light) noexcept;

		/**
		 * Obtains a number of points in the primitive which will act as
		 * light sources.
		 *
		 * Subclasses should override this to provide lighting behaviour.
		 * The return value should be some appropriate sampling of points
		 * from the primitive to act as light sources. By default, the empty
		 * vector is returned.
		 *
		 * The return value must be meaningful if `this->is_light()`. Thus,
		 * if a subclass is not intended to be used as a light,
		 * `set_is_light` should be overriden to enforce this.
		 */
		// TODO: Somehow make this return a generator rather than a concrete
		// vector, in case many sample points are returned. This could also
		// enable the caller to control the number of samples without
		// explicit dependancy on a configuration parameter.
		virtual light_bundle get_lighting_points_impl(::std::size_t) const;

	public:
		/**
		 * Obtains a number of points in the primitive which will act as
		 * light sources.
		 *
		 * If `!this->isLight()`, then nothing is returns. Else, the points
		 * returned by `getLightingPointsImpl()` is returned.
		 */
		light_bundle
		get_lighting_points(::std::size_t const num_samples) const;

		::boost::optional<hit> intersect(ray const& ray, real dist) const
		  noexcept;

	private:
		virtual ::boost::optional<intersect_result>
		intersect_impl(ray const& ray, real dist) const noexcept = 0;

		virtual color get_color_at_impl(vector3 const&,
		                                texture const&) const noexcept;

	public:
		virtual vector3 get_normal_at(vector3 const& position) const
		  noexcept = 0;

		virtual aabb get_aabb() const noexcept = 0;

		virtual bool intersects(aabb const& box) const noexcept = 0;

		color get_color_at(vector3 const& position) const noexcept;
	};

	::std::unique_ptr<primitive> make_sphere(vector3 const& center,
	                                         real radius);

	::std::unique_ptr<primitive> make_plane(vector3 const& normal,
	                                        real depth);

	::std::unique_ptr<primitive> make_box(aabb const& box);
}

#endif // PRIMITIVE_HPP
