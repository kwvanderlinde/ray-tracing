#include "ray.hpp"

#include "vector3.hpp"

namespace raytracer
{
	ray::ray() : m_origin{vector3::zero}, m_direction{vector3::zero}
	{
	}

	ray::ray(vector3 const& origin, vector3 const& direction)
	  : m_origin{origin}, m_direction{direction}
	{
	}

	vector3& ray::origin() noexcept
	{
		return m_origin;
	}

	vector3 const& ray::origin() const noexcept
	{
		return m_origin;
	}

	void ray::origin(vector3 const& origin) noexcept
	{
		m_origin = origin;
	}

	vector3& ray::direction() noexcept
	{
		return m_direction;
	}

	vector3 const& ray::direction() const noexcept
	{
		return m_direction;
	}

	void ray::direction(vector3 const& direction) noexcept
	{
		m_direction = direction;
	}
}
