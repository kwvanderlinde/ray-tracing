#ifndef RAY_HPP
#define RAY_HPP

#include "vector3.hpp"

namespace raytracer
{
	class ray
	{
	private:
		vector3 m_origin;
		vector3 m_direction;

	public:
		ray();

		ray(vector3 const& origin, vector3 const& direction);

		vector3& origin() noexcept;

		vector3 const& origin() const noexcept;

		void origin(vector3 const& origin) noexcept;

		vector3& direction() noexcept;

		vector3 const& direction() const noexcept;

		void direction(vector3 const& direction) noexcept;
	};
}

#endif // RAY_HPP
