#include "renderer.hpp"

#include "aabb.hpp"
#include "affine_transform.hpp"
#include "color.hpp"
#include "grid.hpp"
#include "hit.hpp"
#include "material.hpp"
#include "primitive.hpp"
#include "scene.hpp"
#include "ray.hpp"
#include "surface.hpp"

#include <vlinde/algorithm/non_modifying.hpp>
#include <vlinde/memory/make_unique.hpp>

#include <algorithm>
#include <cmath>
#include <cstddef>
#include <limits>
#include <memory>
#include <random>
#include <vector>

namespace raytracer
{
	struct renderer::raytrace_result
	{
		color color;
		hit hit;
	};

	renderer::renderer(scene const& scene, ::std::size_t trace_depth,
	                   ::std::size_t num_samples) noexcept
	  : m_scene{ ::std::addressof(scene)},
	    m_trace_depth{trace_depth},
	    m_num_samples{num_samples}
	{
	}

	void renderer::render(affine_transform const& transform,
	                      surface& render_target) const
	{
		auto origin = transform(vector3::zero);
		auto p1 = transform({real{-8}, real{4.5}, real{7.5}});
		auto p2 = transform({real{8}, real{4.5}, real{7.5}});
		auto p4 = transform({real{-8}, real{-4.5}, real{7.5}});
		auto delta_x = (p2 - p1) / (render_target.width() - 1);
		auto delta_y = (p4 - p1) / (render_target.height() - 1);

		auto const render_ray = [&](vector3 const& screen_position) {
			vector3 const direction = (screen_position - origin).normalize();
			ray ray{origin, direction};

			auto const& extents = m_scene->grid().extents();
			// Advance ray to box boundary.
			if (!extents.contains(origin)) {
				extents.advance_ray(ray);
			}

			return raytrace(ray, 1, 1.f, m_num_samples);
		};

		auto last_prim_line =
		  ::vlinde::memory::make_unique<primitive const * []>(
		    render_target.width());
		primitive const* last_prim{nullptr};

		for (surface::size_type y = 0; y < render_target.height(); ++y) {
			for (surface::size_type x = 0; x < render_target.width(); ++x) {
				// fire the ray.
				auto const target = p1 + y * delta_y + x * delta_x;

				auto result = render_ray(target);

				// Supersample at primitive boundaries.
				if (result.hit.primitive != last_prim ||
				    result.hit.primitive != last_prim_line[x]) {
					// 3x3 supersampling.
					for (auto ty = -1; ty <= 1; ++ty) {
						for (auto tx = -1; tx <= 1; ++tx) {
							if (tx == 0 && ty == 0)
								continue;

							result.color += render_ray(target + delta_x * tx / 2 +
							                           delta_y * ty / 2).color;
						}
					}
					result.color /= 9.f;
				}

				// cache the current primitive.
				last_prim_line[x] = result.hit.primitive;
				last_prim = result.hit.primitive;

				// fill the surface buffer.
				render_target(x, y) = result.color;
			}
		}
	}

	hit renderer::find_nearest_in(ray const& ray, real const distance,
	                              cell const& cell) const noexcept
	{
		hit result{nullptr, distance, inside::no};

		for (auto const& primitive : cell.primitives) {
			if (auto hit_result =
			      primitive->intersect(ray, result.distance)) {
				result = *hit_result;
			}
		}

		return result;
	}

	hit renderer::find_nearest(ray const& ray,
	                           real const max_distance) const noexcept
	{
		hit result{nullptr, max_distance, inside::no};

		// Setup our control variables.

		auto const& grid = m_scene->grid();
		auto const& cell_size = grid.cell_size();
		::std::size_t step_x, step_y, step_z;
		::std::size_t x, y, z;

		{
			auto cell =
			  (ray.origin() - grid.extents().position()) / cell_size;

			if ((cell.x < 0 || cell.x >= grid.size_x()) ||
			    (cell.y < 0 || cell.y >= grid.size_y()) ||
			    (cell.z < 0 || cell.z >= grid.size_z()))
				return result;

			x = static_cast<decltype(x)>(cell.x);
			y = static_cast<decltype(y)>(cell.y);
			z = static_cast<decltype(z)>(cell.z);
		}

		vector3 cell_bound;
		if (ray.direction().x > 0) {
			step_x = 1;
			cell_bound.x = 1;
		} else {
			step_x = ::std::numeric_limits<::std::size_t>::max();
			cell_bound.x = 0;
		}

		if (ray.direction().y > 0) {
			step_y = 1;
			cell_bound.y = 1;
		} else {
			step_y = ::std::numeric_limits<::std::size_t>::max();
			cell_bound.y = 0;
		}

		if (ray.direction().z > 0) {
			step_z = 1;
			cell_bound.z = 1;
		} else {
			step_z = ::std::numeric_limits<::std::size_t>::max();
			cell_bound.z = 0;
		}

		{
			auto const& extents = grid(x, y, z).extents;
			cell_bound = extents.position() + cell_bound * extents.size();
		}

		auto tmax = cell_bound - ray.origin();
		auto tdelta = cell_size / ray.direction();
		if (ray.direction().x != 0) {
			tmax.x /= ray.direction().x;
			if (step_x != 1)
				// We're going towards negative x.
				tdelta.x = -tdelta.x;
		} else {
			tmax.x = ::std::numeric_limits<real>::infinity();
		}

		if (ray.direction().y != 0) {
			tmax.y /= ray.direction().y;
			if (step_y != 1)
				// We're going towards negative y.
				tdelta.y = -tdelta.y;
		} else {
			tmax.y = ::std::numeric_limits<real>::infinity();
		}

		if (ray.direction().z != 0) {
			tmax.z /= ray.direction().z;
			if (step_z != 1)
				// We're going towards negative z.
				tdelta.z = -tdelta.z;
		} else {
			tmax.z = ::std::numeric_limits<real>::infinity();
		}

		// Step through the grid.
		while (true) {
			auto const& cell = grid(x, y, z);
			auto temp = result;
			auto nearest = find_nearest_in(ray, temp.distance, cell);
			if (nearest.primitive != nullptr)
				temp = nearest;

			// Step to next cell.
			if (tmax.x <= tmax.y && tmax.x <= tmax.z) {
				if (tmax.x > temp.distance) {
					result = temp;
					break;
				}

				x += step_x;
				tmax.x += tdelta.x;

				if (x >= grid.size_x())
					break;
			} else if (tmax.y <= tmax.x && tmax.y <= tmax.z) {
				if (tmax.y > temp.distance) {
					result = temp;
					break;
				}

				y += step_y;
				tmax.y += tdelta.y;

				if (y >= grid.size_y())
					break;
			} else {
				if (tmax.z > temp.distance) {
					result = temp;
					break;
				}

				z += step_z;
				tmax.z += tdelta.z;

				if (z >= grid.size_z())
					break;
			}
		}

		return result;
	}

	color renderer::colorize(ray const& ray, ::std::size_t depth,
	                         real const index, hit const& hit,
	                         real samples) const noexcept
	{
		assert(samples >= 0);

		// +1 to take the ceiling, not the floor.
		auto const num_samples = static_cast<::std::size_t>(samples) + 1;

		color result{0, 0, 0};

		if (hit.primitive == nullptr)
			return result;

		auto const& material = hit.primitive->material();

		if (hit.primitive->is_light()) {
			return result = material.color();
		}

		auto const intersection_point =
		  ray.origin() + ray.direction() * hit.distance;
		auto const N = hit.primitive->get_normal_at(intersection_point);
		auto const prim_color =
		  hit.primitive->get_color_at(intersection_point);

		// calculate lighting
		::vlinde::algorithm::for_each(m_scene->lights(),
		                              [&](primitive const* light) {
			auto lighting_points = light->get_lighting_points(num_samples);

			auto is_occluded = [&](vector3 const& point) {
				auto const light_displacement = point - intersection_point;
				auto const light_distance = light_displacement.length();
				auto const light_direction =
				  light_displacement / light_distance;

				class ray r(intersection_point + light_direction * epsilon,
				            light_direction);

				auto nearest = find_nearest(r, light_distance);
				return nearest.primitive != nullptr &&
				       nearest.primitive != light;
			};

			// Calculate shadows.

			if (lighting_points.samples.empty())
				// Default to the center point.
				lighting_points.samples.push_back(lighting_points.center);

			auto size = lighting_points.samples.size();
			auto shade_count = size;
			for (auto const& point : lighting_points.samples) {
				if (is_occluded(point)) {
					--shade_count;
				}
			}
			auto shade = static_cast<real>(shade_count) / size;
			shade = clamp(shade, real{0}, real{1});

			if (shade <= 0)
				return;

			// We'll need these later for the Phong model.
			auto const L =
			  (lighting_points.center - intersection_point).normalize();
			auto const NdotL = N.dot(L);

			// Calculate diffuse lighting.
			if (material.diffuse() > 0 && NdotL > 0) {
				auto diff = NdotL * material.diffuse() * shade;
				result += diff * prim_color * light->material().color();
			}

			// Calculate specular highlight.
			// Reflect the light ray.
			if (material.specular() > 0) {
				vector3 R = L - 2 * N.dot(L) * N;
				auto dot = ray.direction().dot(R);
				if (dot > 0) {
					auto spec = ::std::pow(dot, material.specular_exponent()) *
					            material.specular() * shade;
					result += spec * light->material().color();
				}
			}
		});

		// trace reflected ray.
		real refl = material.reflection();
		if (refl > 0 && depth < m_trace_depth) {
			// We explicitly normalize R because of possible rounding error.
			auto R =
			  (ray.direction() - 2 * ray.direction().dot(N) * N).normalize();

			// Choose between diffuse and perfect reflection.
			auto drefl = material.diffuse_reflection();
			if (drefl > 0 && depth < 2) {
				// diffuse reflection

				// Choose a basic vector which is least likely to be closely
				// aligned to R.
				vector3 aux;
				{
					auto const x = ::std::abs(R.x);
					auto const y = ::std::abs(R.y);
					auto const z = ::std::abs(R.z);
					auto const min = ::std::min({x, y, z});
					if (x == min)
						aux = vector3::unit_x;
					else if (y == min)
						aux = vector3::unit_y;
					else if (z == min)
						aux = vector3::unit_z;
					else
						assert(false);
				}

				// Generate an orthonormal reference frame {R, R1, R2}.
				auto R1 = R.cross(aux).normalize();
				auto R2 = R.cross(R1).normalize();

				// Sample a bivariate gaussian in the plane {R1, R2}.
				// Bivariate empirical rule: 39.4/86.5/98.9.
				// So this distribution gives us 98.9% of points within drefl.
				::std::normal_distribution<real> dist{0, drefl / 3};

				color total{0, 0, 0};
				for (::std::size_t sample = 0; sample < num_samples; ++sample) {

					auto b = dist(generator);
					auto a = dist(generator);

					auto const direction = (R + R1 * a + R2 * b).normalize();
					class ray reflected(intersection_point + epsilon * direction,
					                    direction);
					auto refl_result =
					  raytrace(reflected, depth + 1, index, samples / 4);
					total += refl * refl_result.color * prim_color;
				}
				result += total / num_samples;

			} else {
				// perfect reflection
				class ray reflected(intersection_point + epsilon * R, R);

				auto refl_result =
				  raytrace(reflected, depth + 1, index, samples / 2);
				result += refl * refl_result.color * prim_color;
			}
		}

		// trace refracted ray.
		real refr = material.refraction();
		if (refr > 0 && depth < m_trace_depth) {
			real rindex = material.refraction_index();
			auto n = index / rindex;

			auto flipped = hit.inside == inside::yes ? -N : N;

			auto cosI = -flipped.dot(ray.direction());
			auto cosT2 = 1 - n * n * (1 - cosI * cosI);
			if (cosT2 > 0) {
				auto T = n * ray.direction() +
				         (n * cosI - ::std::sqrt(cosT2)) * flipped;
				class ray refracted(intersection_point + T * epsilon, T);
				auto refr_result =
				  raytrace(refracted, depth + 1, rindex, samples / 2);

				// Apply Beer's law.
				auto const absorbance =
				  -material.color() * 0.15f * refr_result.hit.distance;
				color const transparency{ ::std::exp(absorbance.r),
				                          ::std::exp(absorbance.g),
				                          ::std::exp(absorbance.b)};
				result += refr_result.color * transparency;
			}
		}

		return result;
	}

	renderer::raytrace_result
	renderer::raytrace(ray const& ray, ::std::size_t const depth,
	                   real const index, real samples) const noexcept
	{
		raytrace_result result{color::black,
		                       hit{nullptr,
		                           ::std::numeric_limits<real>::infinity(),
		                           inside::no}};

		if (depth > m_trace_depth)
			return result;

		result.hit = find_nearest(ray, result.hit.distance);

		result.color = colorize(ray, depth, index, result.hit, samples);

		return result;
	}
}
