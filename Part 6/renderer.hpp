#ifndef RENDERER_HPP
#define RENDERER_HPP

#include "common.hpp"

#include <cstddef>

namespace raytracer
{
  class affine_transform;
  struct cell;
  class color;
  struct hit;
  class ray;
  class scene;
  class surface;

	class renderer
	{
	private:
    struct raytrace_result;

		scene const* m_scene;
		::std::size_t m_trace_depth;
		::std::size_t m_num_samples;

	public:
		renderer(scene const& scene, ::std::size_t trace_depth,
		         ::std::size_t num_samples) noexcept;

		void render(affine_transform const& transform,
		            surface& render_target) const;

  private:
		hit find_nearest_in(ray const& ray, real const distance,
		                    cell const& cell) const noexcept;

		hit find_nearest(ray const& ray, real const max_distance) const
		  noexcept;

		color colorize(ray const& ray, ::std::size_t depth,
		               real const index, hit const& hit,
		               real samples) const noexcept;

		raytrace_result raytrace(ray const& ray, ::std::size_t const depth,
		                         real const index, real samples) const
		  noexcept;
	};
}

#endif // RENDERER_HPP
