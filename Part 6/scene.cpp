#include "scene.hpp"

#include "aabb.hpp"
#include "common.hpp"
#include "grid.hpp"
#include "primitive.hpp"
#include "vector3.hpp"

#include <vlinde/algorithm/non_modifying.hpp>

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <memory>
#include <vector>

namespace raytracer
{
	scene::primitive_range_impl::primitive_range_impl(
	  decltype(m_primitives) const& primitives) noexcept
	  : m_first{primitives.data()},
	    m_last{primitives.data() + primitives.size()}
	{
	}

	bool scene::primitive_range_impl::empty() const noexcept
	{
		return m_first == m_last;
	}

	scene::primitive_range_impl::reference
	scene::primitive_range_impl::front() const noexcept
	{
		assert(!this->empty());

		return m_first->get();
	}

	void scene::primitive_range_impl::pop_front() noexcept
	{
		assert(!this->empty());

		++m_first;
	}

	scene::light_range_impl::light_range_impl(decltype(m_lights) const
	                                          & lights) noexcept
	  : m_first{lights.data()},
	    m_last{lights.data() + lights.size()}
	{
	}

	bool scene::light_range_impl::empty() const noexcept
	{
		return m_first == m_last;
	}

	scene::light_range_impl::reference
	scene::light_range_impl::front() const noexcept
	{
		assert(!this->empty());

		return *m_first;
	}

	void scene::light_range_impl::pop_front() noexcept
	{
		assert(!this->empty());

		++m_first;
	}

	scene::scene()
	  : m_primitives{}, m_lights{},
	    m_grid{aabb{{-14, -5, -6}, {28, 13, 36}}, grid_size,
	           grid_size,                         grid_size}
	{
	}

  scene::~scene() = default;

	void scene::build_grid()
	{
		auto const& cell_size = m_grid.cell_size();
		auto const& extents = m_grid.extents();
		auto const& p1 = extents.position();

		::vlinde::algorithm::for_each(
		  this->primitives(), [&, this](primitive const* primitive) {
			  if (primitive->is_light())
				  m_lights.push_back(primitive);

			  auto bounds = primitive->get_aabb();
			  auto const& v1 = bounds.position();
			  auto const& v2 = bounds.position() + bounds.size();

			  // Find out which cells could contain the primitive.
			  auto diff1 = (v1 - p1) / cell_size;
			  auto diff2 = (v2 - p1) / cell_size;

			  diff1.x = clamp<real>(diff1.x, 0, m_grid.size_x() - 1);
			  diff1.y = clamp<real>(diff1.y, 0, m_grid.size_y() - 1);
			  diff1.z = clamp<real>(diff1.z, 0, m_grid.size_z() - 1);

			  diff2.x = clamp<real>(diff2.x, 0, m_grid.size_x() - 1);
			  diff2.y = clamp<real>(diff2.y, 0, m_grid.size_y() - 1);
			  diff2.z = clamp<real>(diff2.z, 0, m_grid.size_z() - 1);

			  // Get indices from the diffs.
			  auto x1 = static_cast<::std::size_t>(diff1.x);
			  auto y1 = static_cast<::std::size_t>(diff1.y);
			  auto z1 = static_cast<::std::size_t>(diff1.z);

			  auto x2 = static_cast<::std::size_t>(diff2.x) + 1;
			  auto y2 = static_cast<::std::size_t>(diff2.y) + 1;
			  auto z2 = static_cast<::std::size_t>(diff2.z) + 1;

			  for (auto z = z1; z < z2; ++z) {
				  for (auto y = y1; y < y2; ++y) {
					  for (auto x = x1; x < x2; ++x) {
						  auto& cell = m_grid(x, y, z);

						  // Do an accurate cell/primitive intersection.
						  if (primitive->intersects(cell.extents)) {
							  // Add the primitive to the cell.
							  cell.primitives.push_back(primitive);
						  }
					  }
				  }
			  }
			});
	}

	scene::scene(scene&&) = default;

	scene& scene::operator=(scene&&) = default;

	grid const& scene::grid() const noexcept
	{
		return m_grid;
	}

	void scene::swap(scene& other)
	{
		using std::swap;
		swap(m_primitives, other.m_primitives);
		swap(m_grid, other.m_grid);
	}

	void swap(scene& first, scene& second)
	{
		first.swap(second);
	}

	scene::primitive_range scene::primitives() const
	{
		return primitive_range{m_primitives};
	}

	scene::light_range scene::lights() const
	{
		return light_range{m_lights};
	}
}
