#ifndef SCENE_HPP
#define SCENE_HPP

#include "aabb.hpp"
#include "common.hpp"
#include "grid.hpp"
#include "vector3.hpp"

#include <memory>
#include <vector>

namespace raytracer
{
  class primitive;

	class scene
	{
	private:
		::std::vector<::std::unique_ptr<primitive const>> m_primitives;
		::std::vector<primitive const*> m_lights;
		grid m_grid;

		class primitive_range_impl
		{
		private:
			::std::unique_ptr<primitive const> const* m_first;
			::std::unique_ptr<primitive const> const* m_last;

		public:
			explicit primitive_range_impl(decltype(m_primitives) const
			                              & primitives) noexcept;

			using reference = primitive const*;

			bool empty() const noexcept;

			reference front() const noexcept;

			void pop_front() noexcept;
		};

		class light_range_impl
		{
		private:
			primitive const* const* m_first;
			primitive const* const* m_last;

		public:
			explicit light_range_impl(decltype(m_lights) const
			                          & lights) noexcept;

			using reference = primitive const*;

			bool empty() const noexcept;

			reference front() const noexcept;

			void pop_front() noexcept;
		};

		void build_grid();

	public:
		using primitive_range = primitive_range_impl;

		using light_range = light_range_impl;

		scene();

		template <typename InputIterator>
		scene(InputIterator first, InputIterator last)
		  : m_primitives{first, last}, m_lights{},
		    m_grid{aabb{{-14, -5, -6}, {28, 13, 36}}, grid_size,
		           grid_size,                         grid_size}
		{
			this->build_grid();
		}

    ~scene();

		scene(scene&&);

		scene& operator=(scene&&);

		grid const& grid() const noexcept;

		void swap(scene& other);

		friend void swap(scene& first, scene& second);

		primitive_range primitives() const;

		light_range lights() const;
	};
}

#endif // SCENE_HPP
