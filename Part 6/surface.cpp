#include "surface.hpp"

#include "color.hpp"

#include <vlinde/algorithm/modifying.hpp>
#include <vlinde/memory/make_unique.hpp>
#include <vlinde/range/repeat.hpp>

#include <algorithm>
#include <cassert>
#include <cstring>
#include <limits>
#include <memory>

namespace raytracer
{
	surface::surface() : surface{0, 0}
	{
	}

	surface::surface(size_type width, size_type height)
	  : m_width{width}, m_height{height}, m_size{width * height},
	    m_buffer{ ::vlinde::memory::make_unique<color[]>(m_size)}
	{
		this->clear(color::black);
	}

	surface::surface(surface const& other)
	  : surface{other.m_width, other.m_height}
	{
		// no more exceptions may be thrown (not that it matters!)
		// color is trivially copyable
		auto const byte_count = m_size * sizeof(color);
		::std::memcpy(m_buffer.get(), other.m_buffer.get(), byte_count);
	}

	surface::surface(surface&&) noexcept = default;

	surface& surface::operator=(surface const& other)
	{
		// copy and swap.
		surface temp{other};
		this->swap(temp);
		return *this;
	}

	surface& surface::operator=(surface&&) noexcept = default;

	surface::size_type surface::width() const noexcept
	{
		return m_width;
	}

	surface::size_type surface::height() const noexcept
	{
		return m_height;
	}

	surface::size_type surface::size() const noexcept
	{
		return m_size;
	}

	surface::size_type surface::max_size() const noexcept
	{
		return ::std::numeric_limits<size_type>::max();
	}

	bool surface::empty() const noexcept
	{
		return m_size == 0;
	}

	color& surface::operator()(size_type x, size_type y) noexcept
	{
		assert(x < m_width);
		assert(y < m_height);

		return m_buffer[y * m_width + x];
	}

	color const& surface::operator()(size_type x, size_type y) const
	  noexcept
	{
		assert(x < m_width);
		assert(y < m_height);

		return m_buffer[y * m_width + x];
	}

	void surface::clear(color const& color)
	{
		using ::vlinde::algorithm::copy;
		using ::vlinde::range::repeat;

		copy(repeat(color), this->all());
	}

	color& surface::operator[](size_type index) noexcept
	{
		return m_buffer[index];
	}

	color const& surface::operator[](size_type index) const noexcept
	{
		return m_buffer[index];
	}

	void surface::swap(surface& other) noexcept
	{
		using std::swap;
		swap(m_buffer, other.m_buffer);
	}

	void swap(surface& first, surface& second) noexcept
	{
		first.swap(second);
	}

	surface::range surface::all() noexcept
	{
		return range{*this};
	}

	surface::const_range surface::all() const noexcept
	{
		return const_range{*this};
	}
}
