#ifndef SURFACE_HPP
#define SURFACE_HPP

#include "color.hpp"

#include <cassert>
#include <cstddef>

namespace raytracer
{
  class color;

	// TODO Add text rendering.
	class surface
	{
	private:
		// TODO Handle `m_buffer == nullptr` as a valid state in methods.

		::std::size_t m_width;
		::std::size_t m_height;
		::std::size_t m_size;
		::std::unique_ptr<color[]> m_buffer;

		template <typename T>
		class range_impl
		{
		private:
			T* m_first;
			T* m_last;

		public:
			range_impl(surface const& surface)
			  : m_first{surface.m_buffer.get()},
			    m_last{m_first + surface.m_size}
			{
			}

			using reference = T&;

			bool empty() const noexcept
			{
				return m_first == m_last;
			}

			reference front() const noexcept
			{
				assert(!empty());

				return *m_first;
			}

			void pop_front() noexcept
			{
				assert(!empty());

				++m_first;
			}
		};

	public:
		using range = range_impl<color>;
		using const_range = range_impl<color const>;

		using size_type = ::std::size_t;

		surface();

		surface(size_type width, size_type height);

		surface(surface const& other);

		surface(surface&&) noexcept;

		surface& operator=(surface const& other);

		surface& operator=(surface&&) noexcept;

		size_type width() const noexcept;

		size_type height() const noexcept;

		size_type size() const noexcept;

		size_type max_size() const noexcept;

		bool empty() const noexcept;

		color& operator()(size_type x, size_type y) noexcept;

		color const& operator()(size_type x, size_type y) const noexcept;

		void clear(color const& color);

		color& operator[](size_type index) noexcept;

		color const& operator[](size_type index) const noexcept;

		void swap(surface& other) noexcept;

		friend void swap(surface& first, surface& second) noexcept;

		range all() noexcept;

		const_range all() const noexcept;
	};
}

#endif // SURFACE_HPP
