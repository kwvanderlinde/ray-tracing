#ifdef PROFILING
#include <google/profiler.h>
#endif

#include <iostream>

#include "affine_transform.hpp"
#include "camera.hpp"
#include "color.hpp"
#include "common.hpp"
#include "engine.hpp"
#include "grid.hpp"
#include "hit.hpp"
#include "material.hpp"
#include "params.hpp"
#include "primitive.hpp"
#include "ray.hpp"
#include "scene.hpp"
#include "surface.hpp"
#include "texture.hpp"
#include "vector3.hpp"

#include <vlinde/algorithm/non_modifying.hpp>
#include <vlinde/memory/make_unique.hpp>

#include <fstream>
#include <type_traits>

raytracer::scene makeScene()
{
	using namespace raytracer;

	std::vector<::std::unique_ptr<primitive>> primitives;

	texture const marble{"textures/marble.tga"};
	texture const wood{"textures/wood.tga"};

	{
		auto floor_plane = make_plane(vector3::unit_y, real{3});
		floor_plane->name("floor plane");
		floor_plane->material().reflection(real{0});
		floor_plane->material().refraction(real{0});
		floor_plane->material().diffuse(real{0.7});
		floor_plane->material().specular(real{0.5});
		// floor_plane->material().diffuse_reflection(real{0.05});
		floor_plane->material().color({real{0.4}, real{0.3}, real{0.3}});
		floor_plane->material().texture(wood);
		floor_plane->material().u_scale(real{0.05});
		floor_plane->material().v_scale(real{0.05});
		primitives.push_back(::std::move(floor_plane));
	}

	{
		auto back_plane = make_plane(-vector3::unit_z, real{22.4});
		back_plane->name("back plane");
		back_plane->material().reflection(real{0});
		back_plane->material().refraction(real{0});
		back_plane->material().diffuse(real{1});
		back_plane->material().texture(marble);
		back_plane->material().u_scale(real{0.1});
		back_plane->material().v_scale(real{0.1});
		back_plane->material().color({real{0.6}, real{0.5}, real{0.5f}});
		primitives.push_back(::std::move(back_plane));
	}

	{
		auto right_plane = make_plane(vector3::unit_x, real{10});
		right_plane->name("right plane");
		right_plane->material().reflection(real{0});
		right_plane->material().refraction(real{0});
		right_plane->material().diffuse(real{1});
		right_plane->material().texture(marble);
		right_plane->material().u_scale(real{0.1});
		right_plane->material().v_scale(real{0.1});
		right_plane->material().color({real{0.5}, real{0.5}, real{0.6}});
		primitives.push_back(::std::move(right_plane));
	}

	{
		auto left_plane = make_plane(-vector3::unit_x, real{10});
		left_plane->name("left plane");
		left_plane->material().reflection(real{0});
		left_plane->material().refraction(real{0});
		left_plane->material().diffuse(real{1});
		left_plane->material().texture(marble);
		left_plane->material().u_scale(real{0.1});
		left_plane->material().v_scale(real{0.1});
		left_plane->material().color({real{0.5}, real{0.5}, real{0.6}});
		primitives.push_back(::std::move(left_plane));
	}

	{
		auto ceiling_plane = make_plane(-vector3::unit_y, real{5.2});
		ceiling_plane->name("ceiling plane");
		ceiling_plane->material().reflection(real{0});
		ceiling_plane->material().refraction(real{0});
		ceiling_plane->material().diffuse(real{1});
		ceiling_plane->material().color({real{0.7}, real{0.7}, real{0.7}});
		primitives.push_back(::std::move(ceiling_plane));
	}

	{
		auto big_sphere =
		  make_sphere(vector3{real{0}, real{0.5}, real{4}}, real{2});
		big_sphere->name("big sphere");
		big_sphere->material().reflection(real{0.05});
		big_sphere->material().refraction(real{0});
		big_sphere->material().diffuse(real{0.5});
		big_sphere->material().specular(real{0.8});
		// big_sphere->material().diffuse_reflection(real{0.3});
		big_sphere->material().texture(marble);
		big_sphere->material().u_scale(real{5});
		big_sphere->material().v_scale(real{1});
		big_sphere->material().color({real{0.8}, real{0.8}, real{0.8}});
		primitives.push_back(::std::move(big_sphere));
	}

	{
		auto small_sphere =
		  make_sphere(vector3{real{-5}, real{-0.8}, real{7}}, real{2});
		small_sphere->name("small sphere");
		small_sphere->material().reflection(real{0.2});
		small_sphere->material().refraction(real{0});
		small_sphere->material().diffuse(real{0.7});
		// small_sphere->material().diffuse_reflection(real{0.6});
		small_sphere->material().texture(marble);
		small_sphere->material().u_scale(real{5});
		small_sphere->material().v_scale(real{1});
		small_sphere->material().color({real{0.7}, real{0.7}, real{1}});
		primitives.push_back(::std::move(small_sphere));
	}

	{
		auto extra_sphere =
		  make_sphere(vector3{real{5}, real{-0.8}, real{7}}, real{2});
		extra_sphere->name("extra sphere");
		extra_sphere->material().reflection(real{0.6});
		extra_sphere->material().refraction(real{0});
		extra_sphere->material().diffuse(real{0.4});
		extra_sphere->material().texture(marble);
		extra_sphere->material().u_scale(real{5});
		extra_sphere->material().v_scale(real{1});
		extra_sphere->material().color({real{0.8}, real{0.8}, real{1}});
		primitives.push_back(::std::move(extra_sphere));
	}

	{
		auto box = make_box(aabb{vector3{real{-1.5}, real{-3.}, real{2.5}},
		                         vector3{real{3}, real{1.5}, real{3}}});
		box->name("box");
		box->material().reflection(real{0});
		box->material().refraction(real{0});
		box->material().diffuse(real{1});
		box->material().color({real{0.7}, real{0.7}, real{0.7}});
		primitives.push_back(::std::move(box));
	}

	{
#if 1 // Area lights.
		auto light1 = make_box(aabb{vector3{real{3}, real{4}, real{-5}},
		                            vector3{real{2}, real{0.1f}, real{2}}});
		auto light2 = make_box(aabb{vector3{real{1}, real{5}, real{-1}},
		                            vector3{real{2}, real{0.1f}, real{2}}});
#else
		auto light1 =
		  make_sphere(vector3{real{4}, real{4}, real{-4}}, real{0.1});
		auto light2 =
		  make_sphere(vector3{real{2}, real{5}, real{-2}}, real{0.1});
#endif
		light1->is_light(true);
		light1->material().color({real{0.7}, real{0.7}, real{0.7}});

		light2->is_light(true);
		light2->material().color({real{0.7}, real{0.7}, real{0.7}});

		primitives.push_back(::std::move(light1));
		primitives.push_back(::std::move(light2));
	}

	return scene{ ::std::make_move_iterator(primitives.begin()),
	              ::std::make_move_iterator(primitives.end())};
}

int main()
{
#ifdef PROFILING
	ProfilerStart("profile/profile.out");
#endif

	using namespace raytracer;

	auto const width = 1920;
	auto const height = 1080;

	// Setup the render surface.
	surface target{width, height};

	// Setup the rendering engine.
	engine engine;
	engine.eye({real{8}, real{-0.5f}, real{-2}});
	engine.target({real{2}, real{-0.5f}, real{4}});
	engine.trace_depth(6);
	engine.samples(8);
	engine.scene(makeScene());

	// render the scene.
	engine.render_to(target);

#ifdef PROFILING
	ProfilerStop();
#endif

	// write the image to a pbm file.
	::std::ofstream out{"test.ppm",
	                    ::std::ios_base::out | ::std::ios_base::binary};
	out << "P6" << ::std::endl << target.width() << " " << target.height()
	    << ::std::endl << "255" << ::std::endl;

	auto const to_byte = [](real value) {
		auto clamped = clamp(real{255} * value, real{0}, real{255});
		return static_cast<::std::uint8_t>(clamped);
	};

	::vlinde::algorithm::for_each(target.all(), [&](color const& color) {
		out << to_byte(color.r) << to_byte(color.g) << to_byte(color.b);
	});

	return 0;
}
