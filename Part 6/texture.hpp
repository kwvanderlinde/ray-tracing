#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include "surface.hpp"

#include <vlinde/memory/make_unique.hpp>

#include <algorithm>
#include <array>
#include <cmath>
#include <fstream>
#include <iterator>
#include <memory>

namespace raytracer
{
	class texture
	{
	private:
		::std::shared_ptr<surface> m_surface;

	public:
		using size_type = surface::size_type;

		texture() : m_surface{ ::std::make_shared<surface>(0, 0)}
		{
		}

		texture(std::string const& file_name)
		{
			// 1 | 00 - 00 | ID length - assumed 0
			// 2 | 01 - 01 | Color map type - assumed 0
			// 3 | 02 - 02 | Image type - assumed 2 (uncompressed true-color)
			// 4 | 03 - 07 | Color map spec (ignored since #2 is assumed 0)
			// 5 | 08 - 17 | Image spec: x-origin(2), y-origin(2), width(2),
			//                           height(2), pixel depth(1) (ass 24/32)
			//                           descriptor (1) - assumed 0
			// 6 |         | Image ID  - assumed empty
			// 7 |         | Color map data - assumed empty
			// 8 |         | Image data - assumed empty
			::std::fstream file{file_name, ::std::ios_base::in |
			                                 ::std::ios_base::binary};

			// Read the 18-byte header data.
			::std::array<::std::uint8_t, 18> header;
			::std::copy_n(::std::istream_iterator<::std::uint8_t>{file},
			              header.size(), header.begin());

			// Construct the width and height data.
			::std::size_t width = header[12] + 256 * header[13];
			::std::size_t height = header[14] + 256 * header[15];

			m_surface = ::std::make_shared<surface>(width, height);

			auto const size = width * height * 3;
			auto buffer =
			  ::vlinde::memory::make_unique<::std::uint8_t[]>(size);
			::std::copy_n(::std::istream_iterator<::std::uint8_t>{file}, size,
			              buffer.get());

			for (::std::size_t j = 0; j < height; ++j) {
				for (::std::size_t i = 0; i < width; ++i) {
					auto index = i + width * j;
					// Apparently, color data is backward!
					auto b = buffer[index * 3] / 255.f;
					auto g = buffer[index * 3 + 1] / 255.f;
					auto r = buffer[index * 3 + 2] / 255.f;
					(*m_surface)(i, j) = color{r, g, b};
				}
			}
		}

		size_type width() const noexcept
		{
			return m_surface->width();
		}

		size_type height() const noexcept
		{
			return m_surface->height();
		}

		/**
		 * Samples the texture using a blinear filter.
		 */
		color operator()(real u, real v) const noexcept
		{
			auto const mod = [](real f) {
				real integral;
				auto result = ::std::modf(f, &integral);
				if (result < 0) {
					result += 1.f;
				}
				return result;
			};

			u = mod(u);
			v = mod(v);

			auto const fu = u * width();
			auto const fv = v * height();

			auto const u1 = static_cast<size_type>(fu) % width();
			auto const v1 = static_cast<size_type>(fv) % height();
			auto const u2 = (u1 + 1) % width();
			auto const v2 = (v1 + 1) % height();

			// Get fractional parts of u and v, for weighting purposes.
			auto const fracu = fu - ::std::floor(fu);
			auto const fracv = fv - ::std::floor(fv);

			// Calculate weights.
			auto w1 = (1 - fracu) * (1 - fracv);
			auto w2 = fracu * (1 - fracv);
			auto w3 = (1 - fracu) * fracv;
			auto w4 = fracu * fracv;

			// Get the four base texels.
			auto c1 = (*m_surface)(u1, v1);
			auto c2 = (*m_surface)(u2, v1);
			auto c3 = (*m_surface)(u1, v2);
			auto c4 = (*m_surface)(u2, v2);

			return c1 * w1 + c2 * w2 + c3 * w3 + c4 * w4;
		}
	};
}

#endif // TEXTURE_HPP
