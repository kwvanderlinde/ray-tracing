#include "vector3.hpp"

#include <cmath>

namespace raytracer
{
	vector3 const vector3::unit_x{1, 0, 0};
	vector3 const vector3::unit_y{0, 1, 0};
	vector3 const vector3::unit_z{0, 0, 1};
	vector3 const vector3::zero{0, 0, 0};

	vector3::vector3() noexcept : vector3{0, 0, 0}
	{
	}

	vector3::vector3(real x_, real y_, real z_) noexcept : x{x_},
	                                                       y{y_},
	                                                       z{z_}
	{
	}

	vector3& vector3::normalize() noexcept
	{
		real const length = this->length();

		x /= length;
		y /= length;
		z /= length;

		return *this;
	}

	real vector3::length() const noexcept
	{
		return ::std::sqrt(this->square_length());
	}

	real vector3::square_length() const noexcept
	{
		return this->dot(*this);
	}

	real vector3::dot(vector3 const& other) const noexcept
	{
		return x * other.x + y * other.y + z * other.z;
	}

	vector3 vector3::cross(vector3 const& other) const noexcept
	{
		real cross_x = y * other.z - z * other.y;
		real cross_y = z * other.x - x * other.z;
		real cross_z = x * other.y - y * other.x;

		return {cross_x, cross_y, cross_z};
	}

	vector3& vector3::operator+=(vector3 const& other) noexcept
	{
		x += other.x;
		y += other.y;
		z += other.z;

		return *this;
	}

	vector3& vector3::operator-=(vector3 const& other) noexcept
	{
		x -= other.x;
		y -= other.y;
		z -= other.z;

		return *this;
	}

	vector3& vector3::operator*=(real f) noexcept
	{
		x *= f;
		y *= f;
		z *= f;

		return *this;
	}

	vector3& vector3::operator*=(vector3 const& other) noexcept
	{
		x *= other.x;
		y *= other.y;
		z *= other.z;

		return *this;
	}

	vector3& vector3::operator/=(real f) noexcept
	{
		x /= f;
		y /= f;
		z /= f;

		return *this;
	}

	vector3& vector3::operator/=(vector3 const& other) noexcept
	{
		x /= other.x;
		y /= other.y;
		z /= other.z;

		return *this;
	}

	vector3 vector3::operator-() const noexcept
	{
		return {-x, -y, -z};
	}

	vector3 operator+(vector3 const& a, vector3 const& b) noexcept
	{
		auto result = a;
		result += b;
		return result;
	}

	vector3 operator-(vector3 const& a, vector3 const& b) noexcept
	{
		auto result = a;
		result -= b;
		return result;
	}

	vector3 operator*(vector3 const& a, real f) noexcept
	{
		auto result = a;
		result *= f;
		return result;
	}

	vector3 operator*(vector3 const& a, vector3 const& b) noexcept
	{
		auto result = a;
		result *= b;
		return result;
	}

	vector3 operator*(real f, vector3 const& a) noexcept
	{
		auto result = a;
		result *= f;
		return result;
	}

	vector3 operator/(vector3 const& a, real f) noexcept
	{
		auto result = a;
		result /= f;
		return result;
	}

	vector3 operator/(vector3 const& a, vector3 const& b) noexcept
	{
		auto result = a;
		result /= b;
		return result;
	}
}
