#ifndef VECTOR_HPP
#define VECTOR_HPP

#include "common.hpp"

namespace raytracer
{
	class vector3
	{
	public:
		static vector3 const unit_x;
		static vector3 const unit_y;
		static vector3 const unit_z;
		static vector3 const zero;

		real x;
		real y;
		real z;

		vector3() noexcept;

		vector3(real x_, real y_, real z_) noexcept;

		vector3& normalize() noexcept;

		real length() const noexcept;

		real square_length() const noexcept;

		real dot(vector3 const& other) const noexcept;

		vector3 cross(vector3 const& other) const noexcept;

		vector3& operator+=(vector3 const& other) noexcept;

		vector3& operator-=(vector3 const& other) noexcept;

		vector3& operator*=(real f) noexcept;

		vector3& operator*=(vector3 const& other) noexcept;

		vector3& operator/=(real f) noexcept;

		vector3& operator/=(vector3 const& other) noexcept;

		vector3 operator-() const noexcept;
	};

	vector3 operator+(vector3 const& a, vector3 const& b) noexcept;

	vector3 operator-(vector3 const& a, vector3 const& b) noexcept;

	vector3 operator*(vector3 const& a, real f) noexcept;

	vector3 operator*(vector3 const& a, vector3 const& b) noexcept;

	vector3 operator*(real f, vector3 const& a) noexcept;

	vector3 operator/(vector3 const& a, real f) noexcept;

	vector3 operator/(vector3 const& a, vector3 const& b) noexcept;
}

#endif // VECTOR_HPP
