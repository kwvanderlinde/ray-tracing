This raytracing project is closely based on Jacco Bikker's raytracing
tutorial. The notable differences are that distinct components are
placed in separate header files, macros are avoided, standard C++ naming
conventions are used, and standard return types and const correctness
are used. Additionally, standard algorithms are preferred over hand
written loops or repetitive code.

Bikker's raytracer follows the standard convention of the origin
spawning rays towards a canvas which is placed in front of the world. We
invert this to mimics real life cameras and the human eye. That is, rays
are spawned from the canvas towards the origin (or "lens"). The canvas
is behind the origin in this scheme just like the retina is behind the
lens in the human eye.