import common

env = Environment()

common.use_clang(env)
common.apply_verbose_option(env)

AddOption('--build',
          dest='build_type',
          action='store',
          type='string',
          help='Determines build type: debug, release or profiling',
          default='debug')

def set_build_flags(env):
    # Merge flags doesn't do this.
    env.Append(LINKFLAGS = "-flto")

    # Set the language flags
    env.MergeFlags(env.ParseFlags(
        ['-std=c++11 -stdlib=libc++ -lc++ -lc++abi -pedantic',
         '-L/usr/local/lib -flto']))

    # Set the warning flags
    env.MergeFlags(env.ParseFlags([
        '-Weverything -Wno-c++98-compat -Wno-padded -Wno-float-equal',
        '-Wno-documentation -Wno-documentation-unknown-command',
        '-Wno-global-constructors -Wno-exit-time-destructors -Wno-missing-prototypes',
        '-Wno-weak-vtables']))

    # Set the flags specific to the type of build
    env.MergeFlags(env.ParseFlags(
        dict(debug = '-O0 -g',
             release = '-O3 -DNDEBUG',
             profiling = '-O3 -g -DNDEBUG -lprofiler -DPROFILING',
             massif = '-O3 -g -DNDEBUG'
        ).get(env.GetOption('build_type'))))

set_build_flags(env)

SConscript('SConscript', variant_dir='build', exports='env')
